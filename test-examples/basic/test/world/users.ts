import { Collection, CollectionMember } from "@flyyn/ribbon";

export class UserRef extends CollectionMember<any> {
  protected override collection = () => UserCollection as any;

  public constructor(
    public readonly id: string,
    public readonly email: string,
    public readonly isAdmin: boolean,
  ) {
    super();
  }

  protected uid(): string {
    return this.id;
  }
}

export class UserCollection extends Collection<UserRef> {
  protected override ns = "me.flyyn.ribbon-test-examples.user";
  protected override type = UserRef;

  protected is(id: string, user: UserRef): boolean {
    return user.id === id;
  }

}

export const admin = new UserRef(
  "0000",
  "admin@example.com",
  true,
);

export const standardUser = new UserRef(
  "0001",
  "user@example.com",
  false,
);
