import { MetadataCollection, MetadataEntry, World } from "@flyyn/ribbon";
import { UsersService } from "../../src/users-service";
import { UserCollection } from "./users";

export const DB_OPEN = new MetadataEntry("DB_OPEN", {
  defaultValue: false,
});

/**
 * Tracks if the database file exists, and has the schema initialized.
 */
export const DB_EXISTS = new MetadataEntry("DB_EXISTS", {
  load: async (t) => {
    const lock = t.safeLock(DB_OPEN);
    const db = await UsersService.db();
    const res: undefined | { name: string }
      = await db.get("SELECT name FROM sqlite_master WHERE type='table' AND name='users';");
    const hasTable = res && typeof res.name === "string" && res.name === "users";
    await db.close();
    t.unlock(lock);
  },
});

export const world = World
  .create()
  .addCollection(UserCollection)
  .addCollection(MetadataCollection)
  .loadOnStart([ DB_OPEN, DB_EXISTS ])
  .addActions({
    reset: async () => {
      await UsersService.nuke();
      await UsersService.createTable();
    },
  })
  .finalize();
