import { Runner } from "@flyyn/ribbon";
import { AdminCreateUsers } from "./suites/0010-admin-create-users";
import { UserLogin } from "./suites/0020-user-login";
import { world } from "./world";

Runner
   .world(world)
   .addSuite(AdminCreateUsers)
   .addSuite(UserLogin)
   .run();
