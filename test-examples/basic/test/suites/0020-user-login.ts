import { Suite } from "@flyyn/ribbon";
import { RibbonHelpers } from "@flyyn/ribbon/out/src/RibbonTestUtilities";
import { world } from "../world";
import { AdminCreateUsers } from "./0010-admin-create-users";

export const UserLogin = Suite
  .create(world, "Created users can login")
  .follows(AdminCreateUsers)
  .step("test login", async (t: RibbonHelpers) => {
    t.is(1, 1);
  })
  .complete();
