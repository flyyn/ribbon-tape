import request from "supertest";
import { RibbonAssert, StepResult, Suite } from "@flyyn/ribbon";
import { app } from "../../src/app";
import { world } from "../world";
import { admin, standardUser } from "../world/users";
import { RibbonHelpers } from "@flyyn/ribbon/out/src/RibbonTestUtilities";

export const AdminCreateUsers = Suite
  .create(world, "Admin can create users", {
    tags: ["users", "db"],
  })
  .require({
    exists: {
      "admin": admin,
    },
    lacks: [ standardUser ],
    setup: w => {
      return w.act("reset");
    },
  })
  .step("get 'is admin' flag", async (t: RibbonHelpers) => {
    const isAdmin = false;
    t.isBool(isAdmin);
    return isAdmin;
  })
  .recallableStep("standardUser", "create 'standardUser'", async (t, isAdmin) => {
    const assert: RibbonAssert = t.assert;
    const res = await request(app)
      .post("/users")
      .send({
        requesterId: admin.id,
        id: standardUser.id,
        email: standardUser.email,
        isAdmin,
      })
      .expect(200);
    const body = res.body;
    assert.isType(body, "string");
    return body;
  })
  .step("check stored correctly", async (t, returnedId, assert: RibbonAssert) => {
    const recalled: StepResult<string, boolean> = t.fromStep("standardUser");
    assert.is(true, recalled.successful);
    assert.is(returnedId, recalled.result);
  })
  .complete();
