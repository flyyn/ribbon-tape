import express from "express";
import { json } from "body-parser";
import { UsersService } from "./users-service";

export const app = express();

interface CreateUserBody {
  /**
   * Authentication for the user submitting the request.
   * Yes, this is horribly insecure.
   */
  requesterId: string;
  id: string;
  email: string;
  isAdmin: boolean;
}

app.post("/users", json(), async (req, res) => {
  const { requesterId, id, email, isAdmin }: CreateUserBody = req.body;
  const loggedIn = await UsersService.userExists(requesterId);
  if(!loggedIn) {
    return res.status(401).send("Not logged in or unknown user.");
  }
  const duplicate = await UsersService.userExists(id);
  if(duplicate) {
    return res.status(409).send("User with this ID already exists.");
  }
  const created = await UsersService.createUser(id, email, isAdmin);
  return res.send(created.id);
});
