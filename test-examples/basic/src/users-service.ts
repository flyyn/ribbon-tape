import sqlite from "sqlite3";
import { open } from "sqlite";
import { DB_FILENAME } from "./db";

export interface UserEntity {
  id: string;
  email: string;
  isAdmin: boolean;
}

export class UsersService {

  static db() {
    return open({
      filename: DB_FILENAME,
      driver: sqlite.Database,
    });
  }

  static async userExists(id: string): Promise<boolean> {
    const db = await this.db();
    try {
      const res = await db.get("SELECT email FROM users WHERE id = ?", id);
      await db.close();
      return res.email.length > 0;
    } catch (e) {
      await db.close();
      return false;
    }
  }

  static async getUser(id: string): Promise<UserEntity> {
    const db = await this.db();
    const res = await db.get("SELECT id, email, isAdmin FROM users WHERE id = ?", id);
    await db.close();
    return res;
  }

  static async createUser(id: string, email: string, isAdmin: boolean): Promise<UserEntity> {
    const db = await this.db();
    await db.run("INSERT INTO users (id, email, isAdmin) VALUES (?, ?, ?)", id, email, isAdmin);
    await db.close();
    return this.getUser(id);
  }

  static async nuke(): Promise<void> {
    const db = await this.db();
    await db.run(`DROP TABLE users;`);
    await db.close();
  }

  static async createTable(): Promise<void> {
    const db = await this.db();
    await db.run("CREATE TABLE users (id VARCHAR(255), email VARCHAR(255), isAdmin boolean)");
    await db.close();
  }

}
