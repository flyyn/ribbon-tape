import { performance } from "perf_hooks";
import chalk from "chalk";
import numeral from "numeral";

export class TimeLogger {
  private readonly start: number;
  private _end: number | undefined;

  constructor() {
    this.start = performance.now();
  }

  end(): this {
    this._end = performance.now();
    return this;
  }

  log(
    goodBefore: number,
    scale: "ms" | "s" = "ms",
    averageBefore: number = goodBefore * 3,
    longBefore: number = goodBefore * 6,
    terribleOver: number = goodBefore * 10,
    prefix: string = "(in ",
    suffix: string = `)`,
  ) {
    if(!this._end) {
      throw new ReferenceError(`Cannot log time before stopping timer.`);
    }
    const duration = this._end - this.start;
    const scaledDuration = scale === "ms" ? duration : duration / 1000;
    const color =
      scaledDuration <= goodBefore ? chalk.green :
      scaledDuration <= averageBefore ? chalk.reset :
      scaledDuration <= longBefore ? chalk.yellow :
      scaledDuration < terribleOver ? chalk.red :
        chalk.bgRed;

    const ms = duration;
    if(ms < 800) {
      return color(`${prefix}${numeral(ms).format('0.00')}ms${suffix}`);
    }

    const s = duration / 1000;
    if(s < 8) {
      return color(`${prefix}${numeral(s).format('0.000')}s${suffix}`);
    } else if(s < 50) {
      return color(`${prefix}${numeral(s).format('00.00')}s${suffix}`);
    }

    const m = s / 60;
    if(m < 8) {
      return color(`${prefix}0:${numeral(m).format('0')}:${numeral(s % 60).format('00')}${suffix}`);
    } else if(m < 50) {
      return color(`${prefix}${numeral(m).format('0.0')}m${suffix}`);
    }

    const h = s / 60;
    if(h < 8) {
      return color(`${prefix}${numeral(h).format('0')}:${numeral(m % 60).format('00')}:${numeral(s % 60).format('00')}${suffix}`);
    } else {
      return color(`${prefix}${numeral(h).format('0.0')}${suffix}`);
    }
  }

}
