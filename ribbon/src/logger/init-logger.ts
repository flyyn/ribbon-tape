import { colorInitSection, LogSeverity, randomFamilyColor, RibbonLogger } from "./Logger";

export class RibbonInitLogger {
  public constructor(
    private readonly logger: RibbonLogger,
    private readonly describe: boolean,
    private readonly describeFull: boolean,
  ) {}

  public debug(message: string) {
    this.writeMessage(LogSeverity.DEBUG, message);
  }

  public log(message: string) {
    this.writeMessage(LogSeverity.LOG, message);
  }

  public info(message: string) {
    this.writeMessage(LogSeverity.INFO, message);
  }

  public warn(message: string) {
    this.writeMessage(LogSeverity.WARN, message);
  }

  public error(message: string) {
    this.writeMessage(LogSeverity.ERROR, message);
  }

  private writeMessage(severity: LogSeverity, message: string) {
    if(!this.describe) {
      return;
    }
    this.logger.writeMessage({
      message: `   ${message}`,
      topComponent: "runner:init",
      severity: severity,
    });
  }

  public suite(suiteNumber: number, suiteName: string) {
    return new RibbonInitSectionLogger(this.logger, this.describeFull, suiteNumber, suiteName);
  }
}

export class RibbonInitSectionLogger {
  public constructor(
    private readonly logger: RibbonLogger,
    private readonly v: boolean,
    private readonly suiteNum: number,
    private readonly suiteName: string,
    private readonly subsections: string[] = [],
  ) {}

  public subsection(name: string): RibbonInitSectionLogger {
    return new RibbonInitSectionLogger(
      this.logger,
      this.v,
      this.suiteNum,
      this.suiteName,
      [...this.subsections, name],
    );
  }

  public logSuiteStart() {
    const name = colorInitSection(this.suiteNum, this.suiteName);
    this.info(`Checking suite ${name}`);
  }

  public logSuiteSchedule(durationInfo: string) {
    const name = colorInitSection(this.suiteNum, this.suiteName);
    this.info(`Suite ${name} passed all checks, scheduling ${durationInfo}`, true);
  }

  public debug(message: string) {
    this.writeMessage(LogSeverity.DEBUG, message);
  }

  public log(message: string) {
    this.writeMessage(LogSeverity.LOG, message);
  }

  public info(message: string, bypass=false) {
    this.writeMessage(LogSeverity.INFO, message, bypass);
  }

  public warn(message: string) {
    this.writeMessage(LogSeverity.WARN, message);
  }

  public error(message: string) {
    this.writeMessage(LogSeverity.ERROR, message);
  }

  private writeMessage(severity: LogSeverity, message: string, bypass: boolean = false) {
    if(!bypass && !this.v) {
      return;
    }
    this.logger.writeMessage({
      message: ` ${this.prefix()}${message}`,
      topComponent: `runner:init ${this.suiteNum}`,
      severity: severity,
    });
  }

  private prefix(): string {
    const sects = this.subsections;
    const out: string[] = [];
    if(sects.length === 0) {
      return "";
    }
    if(sects.length > 0) {
      out.push(randomFamilyColor("pink", sects[0], sects[0]));
    }
    if(sects.length > 1) {
      out.push(randomFamilyColor("orange", sects[1], sects[1]));
    }
    return out.join(" ") + " ";
  }

}
