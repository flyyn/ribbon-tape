import { DebugLogger } from "./debug-logger";
import { RibbonLogger } from "./Logger";

export class RibbonStateManagerLogger extends DebugLogger {
  public constructor(
    logger: RibbonLogger,
    describeFull: boolean,
  ) {
    super(logger, "state-manager", " ", describeFull);
  }
}
