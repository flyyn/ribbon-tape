import { LoggerCommon } from "./common";
import { LogSeverity, RibbonLogger } from "./Logger";

export class RibbonRunnerLogger extends LoggerCommon {
  public constructor(
    private readonly logger: RibbonLogger,
    private readonly describe: boolean,
    private readonly describeFull: boolean,
  ) {
    super();
  }

  public debug(message: string, loud: boolean = false) {
    this.writeMessage(LogSeverity.DEBUG, message, loud);
  }

  public log(message: string, loud: boolean = false) {
    this.writeMessage(LogSeverity.LOG, message, loud);
  }

  public info(message: string, loud: boolean = false) {
    this.writeMessage(LogSeverity.INFO, message, loud);
  }

  public warn(message: string, loud: boolean = false) {
    this.writeMessage(LogSeverity.WARN, message, loud);
  }

  public error(message: string, loud: boolean = false) {
    this.writeMessage(LogSeverity.ERROR, message, loud);
  }

  private writeMessage(severity: LogSeverity, message: string, loud: boolean = false) {
    const loudSilence = loud && (!this.describe && !this.describeFull);
    const quietSilence = !loud && !this.describeFull;
    if(loudSilence || quietSilence) {
      return;
    }
    this.logger.writeMessage({
      message,
      topComponent: "runner:between",
      severity: severity,
    });
  }
}
