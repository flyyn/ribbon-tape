import { DebugLogger } from "./debug-logger";
import { RibbonLogger } from "./Logger";

export class RibbonBootLogger extends DebugLogger {
  public constructor(
    logger: RibbonLogger,
    describeFull: boolean,
  ) {
    super(logger, "runner:setup", "  ", describeFull);
  }
}
