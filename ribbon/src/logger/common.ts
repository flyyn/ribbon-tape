import { LoggingFormatter } from "./formatters";

export class LoggerCommon {
  public formatters = new LoggingFormatter();
}
