import chalk from "chalk";
import randomcolor from "randomcolor";
import { PrefixedString } from "../util/prefix";
import { RibbonBootLogger } from "./boot-logger";
import { RibbonInitLogger } from "./init-logger";
import { RibbonRunnerLogger } from "./runner-logger";
import { RunningSuiteLogger } from "./running-suite-logger";
import { RibbonStateManagerLogger } from "./state-logger";

const DAYS_OF_WEEK = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

export enum LogSeverity {
  VERBOSE,
  TRACE,
  DEBUG,
  LOG,
  INFO,
  WARN,
  ERROR,
  FATAL,
}

const SEVERITY_LABELS: Record<LogSeverity, string> = {
  [LogSeverity.VERBOSE]: "VERBOSE",
  [LogSeverity.TRACE]: "TRACE",
  [LogSeverity.DEBUG]: "DEBUG",
  [LogSeverity.LOG]: "LOG",
  [LogSeverity.INFO]: "INFO",
  [LogSeverity.WARN]: "WARN",
  [LogSeverity.ERROR]: "ERROR",
  [LogSeverity.FATAL]: "FATAL",
};

const SEVERITY_LABEL_COLOR: Record<LogSeverity, (...args: string[]) => string> = {
  [LogSeverity.VERBOSE]: chalk.gray,
  [LogSeverity.TRACE]: chalk.bgWhite.black,
  [LogSeverity.DEBUG]: chalk.cyan,
  [LogSeverity.LOG]: chalk.reset,
  [LogSeverity.INFO]: chalk.bold,
  [LogSeverity.WARN]: chalk.yellow,
  [LogSeverity.ERROR]: chalk.red,
  [LogSeverity.FATAL]: chalk.bgRed,
};

function msgBodyColor(severity: LogSeverity): (...args: string[]) => string {
  if(severity <= LogSeverity.DEBUG) {
    return chalk.grey;
  }
  if(severity >= LogSeverity.ERROR) {
    return chalk.red;
  }
  if(severity === LogSeverity.WARN) {
    return chalk.yellow;
  }
  return chalk.reset;
}

export type TopComponentName
  = "runner"
  | "runner:init"
  | "runner:setup"
  | "runner:between"
  | "state-manager"
  | PrefixedString<"runner:init ", string>
  | PrefixedString<"suite:", string>;

export interface BaseRibbonLogMessage<Severity extends LogSeverity | undefined, Data extends object> {
  severity: Severity;
  topComponent: TopComponentName;
  messageType?: string;
  data?: Data;
}

export interface RibbonLoggerFinalizedMessage<Severity extends LogSeverity | undefined> extends BaseRibbonLogMessage<Severity, any> {
  message: string;
}

export function randomFamilyColor(hue: string, seed: string | number, text: string): string {
  const color = randomcolor({ hue, seed });
  return chalk.hex(color)(text);
}

export function colorInitSection(seed: string | number, text: string): string {
  return randomFamilyColor("green", seed, text);
}

function formatTopComponent(topComponent: TopComponentName): string {
  if(topComponent === "runner") {
    return chalk.bgMagenta(topComponent);
  } else if(topComponent === "runner:init") {
    return chalk.cyan(topComponent);
  } else if(topComponent === "runner:setup") {
    return chalk.blue(topComponent);
  } else if(topComponent === "runner:between") {
    return chalk.magenta(topComponent);
  } else if(topComponent.indexOf("runner:init ") === 0) {
    const initStage = topComponent.replace(/^runner:init\s/, '');
    const color = randomcolor({ hue: "green", seed: initStage });
    return `${chalk.cyan("runner:init ")}${colorInitSection(initStage, initStage)}`;
  } else {
    const color = randomcolor({ hue: "blue", seed: topComponent });
    return chalk.hex(color)(topComponent);
  }
}

export class RibbonLogger {

  public stateManagerLogger(describe: boolean, describeFull: boolean) {
    return new RibbonStateManagerLogger(this, describeFull);
  }

  public initLogger(describe: boolean, describeFull: boolean) {
    return new RibbonInitLogger(this, describe, describeFull);
  }

  public bootLogger(describe: boolean, describeFull: boolean) {
    return new RibbonBootLogger(this, describeFull);
  }

  public betweenLogger(describe: boolean, describeFull: boolean) {
    return new RibbonRunnerLogger(this, describe, describeFull);
  }

  public suite(num: number, name: string, describeFull: boolean) {
    return new RunningSuiteLogger(this, num, name, describeFull);
  }

  public log(message: string, opts?: BaseRibbonLogMessage<LogSeverity.LOG | undefined, any>) {
    const msg: RibbonLoggerFinalizedMessage<LogSeverity.LOG> = {
      severity: LogSeverity.LOG,
      topComponent: "runner",
      message,
      data: opts ? opts.data : undefined,
    };
    this.writeMessage(msg);
  }

  public writeMessage(message: RibbonLoggerFinalizedMessage<LogSeverity>) {
    const sev = message.severity;
    const time = new Date();
    const formattedDate = `${DAYS_OF_WEEK[time.getDay()]} ${time.getFullYear()}-${time.getMonth() + 1}-${time.getDate()}`;
    function segment(contents: string): string {
      return `${chalk.dim('[')}${contents}${chalk.dim(']')}`;
    }
    const date = segment(chalk.dim(formattedDate));
    const severityLabelBase = segment(SEVERITY_LABEL_COLOR[sev](SEVERITY_LABELS[sev]));
    const severityPadLength = SEVERITY_LABELS[LogSeverity.VERBOSE].length - SEVERITY_LABELS[sev].length;
    const severity = `${severityLabelBase}${" ".repeat(severityPadLength)}`;
    const top = segment(formatTopComponent(message.topComponent));
    const prefix = `${date} ${severity} ${top} `;
    const followingPrefix = " ".repeat(prefix.length);
    const [firstLine, ...following] = msgBodyColor(sev)(message.message).split("\n");
    const lines = [
      `${prefix}${firstLine}`,
      ...following.map(l => `${followingPrefix}${l}`),
    ].join("\n");
    if(sev <= LogSeverity.INFO) {
      console.log(lines);
    } else {
      console.error(lines);
    }
  }

}
