import chalk from "chalk";
import randomcolor from "randomcolor";
import { TrackedIdentifier } from "../state/util/tracked-id";

export class LoggingFormatter {
  public randomFamilyColor(hue: string, seed: string | number, text: string): string {
    const color = randomcolor({ hue, seed });
    return chalk.hex(color)(text);
  }

  public trackedIdName(id: TrackedIdentifier): string {
    return this.randomFamilyColor("orange", id.typeName, id.typeName);
  }

  public trackedIdHash(id: TrackedIdentifier): string {
    const prefix = id.typePrefix;
    const hash = id.hash();
    const shortHash = hash.substr(0, 5);
    const str = `[${prefix}${shortHash}]`;
    return chalk.yellow(str);
  }

  public trackedId(id: TrackedIdentifier): string {
    return `${this.trackedIdName(id)}${chalk.dim("#")}${id.name()} ${this.trackedIdHash(id)}`;
  }
}
