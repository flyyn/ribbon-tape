import { LoggerCommon } from "./common";
import { LogSeverity, RibbonLogger, TopComponentName } from "./Logger";

/**
 * A logger that can be enabled or disabled, and has a preset component
 * attached to all messages.
 */
export class DebugLogger extends LoggerCommon {
  public constructor(
    private readonly logger: RibbonLogger,
    private readonly topComponent: TopComponentName,
    private readonly messageBodyPrefix: string,
    private readonly enabled: boolean,
  ) {
    super();
  }

  public verbose(message: string) {
    this.writeMessage(LogSeverity.VERBOSE, message);
  }

  public trace(message: string) {
    this.writeMessage(LogSeverity.TRACE, message);
  }

  public debug(message: string) {
    this.writeMessage(LogSeverity.DEBUG, message);
  }

  public log(message: string) {
    this.writeMessage(LogSeverity.LOG, message);
  }

  public info(message: string) {
    this.writeMessage(LogSeverity.INFO, message);
  }

  public warn(message: string) {
    this.writeMessage(LogSeverity.WARN, message);
  }

  public error(message: string) {
    this.writeMessage(LogSeverity.ERROR, message);
  }

  private writeMessage(severity: LogSeverity, message: string) {
    if(!this.enabled) {
      return;
    }
    this.logger.writeMessage({
      message: `${this.messageBodyPrefix}${message}`,
      topComponent: this.topComponent,
      severity: severity,
    });
  }
}
