import { DebugLogger } from "./debug-logger";
import { RibbonLogger } from "./Logger";

export class RunningSuiteLogger extends DebugLogger {
  public constructor(
    logger: RibbonLogger,
    suiteNum: number,
    suiteName: string,
    describeFull: boolean,
  ) {
    super(logger, `suite:${suiteName}`, "", describeFull);
  }
}
