export type SuiteOptionsBaseFlag = string | number | (string | number)[];

export interface SuiteOptions {
  tags?: string[],
  // flags?: Record<string, SuiteOptionsBaseFlag | Record<string | number, SuiteOptionsBaseFlag>>,
}
