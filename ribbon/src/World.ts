import { Collection, CollectionClass, CollectionMember } from "./Collection";
import { FirstSystemAction } from "./SystemAction";

export type ActionCallback<T> = () => Promise<T>;
export type WorldActions = { [actionId: string]: any };
export type StoredActions<Actions extends WorldActions> = { [ActionID in keyof Actions]: () => Promise<Actions[ActionID]> };

/**
 * Tracks the current state of the test environment.
 */
export class World<
  CollectionContents extends CollectionMember<any>,
  Collections extends CollectionClass<CollectionContents>[],
  Actions extends WorldActions,
> {
  static create(): BuildableWorld<never, [], {}> {
    return new BuildableWorld([], [], {});
  }

  public readonly Collections: Collections;

  public constructor(
    protected readonly collections: Collections,
    protected readonly loadOnBoot: CollectionContents[],
  ) {}
}

export class BuildableWorld<
  CollectionContents extends CollectionMember<any>,
  Collections extends CollectionClass<CollectionContents>[],
  Actions extends WorldActions
> extends World<CollectionContents, Collections, Actions> {

  public constructor(
    collections: Collections,
    loadOnBoot: CollectionContents[],
    private readonly actions: StoredActions<WorldActions>,
  ) {
    super(collections, loadOnBoot);
  }

  /**
   * Add a {@link Collection}, which organizes external entities in the world that you might interact with.
   *
   * (Must supply a non-constructed class)
   * @param c The collection to add
   */
  public addCollection<
    T extends CollectionMember<any>,
    C extends CollectionClass<T>,
  >(c: C): BuildableWorld<CollectionContents | T, [...Collections, C], Actions> {
    return new BuildableWorld<CollectionContents | T, [...Collections, C], Actions>([
      ...this.collections,
      c,
    ], this.loadOnBoot, this.actions);
  }

  /**
   * Add a set of {@link Collection} entities that should initialize before any suites or tests run.
   *
   * Entities should be selected carefully - this is done without specific suites in scope,
   * so you may load things without a suite that needs them.
   *
   * On the other hand, initializing key elements that are used at the start suite setups
   * can provide an extra layer of safety, as these entities will be loaded no matter what.
   */
  public loadOnStart<T extends CollectionContents>(entities: T[]) {
    return new BuildableWorld<CollectionContents, Collections, Actions>(this.collections, entities, this.actions);
  }

  public addActions<AddedActions extends WorldActions>(actions: StoredActions<AddedActions>): BuildableWorld<
    CollectionContents,
    Collections,
    Actions & AddedActions
  > {
    return new BuildableWorld(this.collections, this.loadOnBoot, {
      ...this.actions,
      ...actions,
    });
  }

  public finalize(): BuiltWorld<CollectionContents, Collections, Actions> {
    return new BuiltWorld(this.collections, this.loadOnBoot, this.actions);
  }

}

/**
 * A version of the world that can't have additional elements
 * globally defined.
 */
export class BuiltWorld<
  CollectionContents extends CollectionMember<any>,
  Collections extends CollectionClass<CollectionContents>[],
  Actions extends WorldActions
> extends World<CollectionContents, Collections, Actions> {

  public constructor(
    collections: Collections,
    loadOnBoot: CollectionContents[],
    private readonly actions: StoredActions<WorldActions>,
  ) {
    super(collections, loadOnBoot);
  }

  public act<
    Result extends { [key in keyof Actions]?: Actions[key] },
    ActionId extends keyof Result,
  >(actionId: ActionId): FirstSystemAction<
    Result,
    this,
    ActionId,
    Result[ActionId]
  > {
    return new FirstSystemAction(this, actionId, (this.actions as any)[actionId]);
  }

}
