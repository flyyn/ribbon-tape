import { CollectionClass, CollectionMember } from "../Collection";
import { RibbonLogger } from "../logger/Logger";
import { TimeLogger } from "../logger/TimeLogger";
import { ErrorOutput, RunOptions } from "../RunOptions";
import { RunOptionsCLIParser } from "../RunOptionsCLIParser";
import { StateService } from "../state/StateService";
import { FinalizedSuite } from "../Suite";
import { World } from "../World";
import { evalSuite } from "./eval-suite";
import { initialEntityLoad, registerCollections } from "./init-world";
import { runSuite } from "./run-suite";
import { SuiteCollector } from "./suite-collector";

interface SuiteResult {
  suite: FinalizedSuite<any, any, any, any, any, any>[];
  duringBuild: ErrorOutput;
  started: boolean;
  finished: boolean;
};

/**
 * Test runner responsible for handling multiple suites.
 */
export class Runner<
  CollectionContents extends CollectionMember<any>,
  Collections extends CollectionClass<CollectionContents>[],
  W extends World<CollectionContents, Collections, any>,
  Suites extends FinalizedSuite<any, any, any, any, any, any>[],
> {

  public static world<
    CollectionContents extends CollectionMember<any>,
    Collections extends CollectionClass<CollectionContents>[],
    W extends World<CollectionContents, Collections, any>>(
    world: W,
  ): Runner<CollectionContents, Collections, W, []> {
    return new Runner(world, []);
  }

  public constructor(
    private readonly world: W,
    private readonly suites: Suites,
  ) {}

  public addSuite<S extends FinalizedSuite<any, any, any, any, any, any>>(
    suite: S,
  ): Runner<CollectionContents, Collections, W, [...Suites, S]> {
    return new Runner(this.world, [...this.suites, suite]);
  }

  public async run(options: RunOptions = RunOptionsCLIParser(process.argv)) {
    const completeTimer = new TimeLogger();
    const log = new RibbonLogger();
    const initLog = log.initLogger(
      options.describe === true || options.fullDescribe === true,
      options.fullDescribe === true,
    );
    const collected = new SuiteCollector();

    const startSuitesCheck = new TimeLogger();
    initLog.info(`Evaluating ${this.suites.length} suites.`);
    for (let suiteNum = 0; suiteNum < this.suites.length; suiteNum++) {
      const suite = this.suites[suiteNum];

      evalSuite(options, collected, suite, initLog, initLog.suite(suiteNum, suite.name));
    }
    startSuitesCheck.end();
    initLog.debug(`${collected.countScheduled()} suites scheduled.`);
    initLog.info(`Evaluated ${this.suites.length} suites ${startSuitesCheck.log(this.suites.length * 20, "ms")}`);

    const bootLog = log.bootLogger(options.describe === true, options.fullDescribe === true);
    const betweenLog = log.betweenLogger(options.describe === true, options.fullDescribe === true);

    const worldSetupTimer = new TimeLogger();
    betweenLog.info(`Setting up world.`, true);

    const world = this.world;
    const stateService = new StateService(log.stateManagerLogger(options.describe === true, options.fullDescribe === true));

    registerCollections(stateService, world["collections"], bootLog);
    await initialEntityLoad(stateService, world["loadOnBoot"], bootLog);

    worldSetupTimer.end();
    betweenLog.info(`Completed world initialization ${worldSetupTimer.log(100, "ms")}`, true);

    const runTimer = new TimeLogger();
    const scheduled = collected.getScheduled();
    betweenLog.info(`Running ${scheduled.length} suites.`);

    for (let suiteNum = 0; suiteNum < scheduled.length; suiteNum++) {
      const suite = scheduled[suiteNum];

      await runSuite(options, collected, suite, betweenLog, log.suite(suiteNum, suite.name, options.fullDescribe === true));
    }
  }

}
