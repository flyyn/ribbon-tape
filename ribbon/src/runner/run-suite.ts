import { RibbonRunnerLogger } from "../logger/runner-logger";
import { RunningSuiteLogger } from "../logger/running-suite-logger";
import { TimeLogger } from "../logger/TimeLogger";
import { RunOptions } from "../RunOptions";
import { FinalizedSuite } from "../Suite";
import { SuiteCollector } from "./suite-collector";

export async function runSuite(
  options: RunOptions,
  collected: SuiteCollector,
  suite: FinalizedSuite<any, any, any, any, any, any>,
  betweenLog: RibbonRunnerLogger,
  log: RunningSuiteLogger,
) {
  betweenLog.info(`Starting suite ${suite.name}`, true);
  const suiteRun = new TimeLogger();

  betweenLog.debug("Checking required items to exist.");
  const requiredItemsToExist = suite["requiredItemsToExist"];
  console.log(requiredItemsToExist);
}
