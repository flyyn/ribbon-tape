import { ErrorOutput } from "../RunOptions";
import { FinalizedSuite, Suite } from "../Suite";

type QuerySuite = Suite<any, any, any>;
type CollectedSuite = FinalizedSuite<any, any, any, any, any, any>;

class StoredSuite {

  private scheduled?: boolean;
  private presetStatus?: ErrorOutput;

  public constructor(
    private readonly suite: CollectedSuite,
  ) {}

  public getSuite(): CollectedSuite {
    return this.suite;
  }

  public is(suite: QuerySuite): boolean {
    return this.suite.name === suite.name;
  }

  public isScheduled(): boolean {
    return this.scheduled === true;
  }

  public schedule() {
    if(this.scheduled) { return; }
    if(this.presetStatus) {
      throw new Error(`Can't schedule a suite that has a preset status.`);
    }
    this.scheduled = true;
  }

  public mark(presetStatus: ErrorOutput) {
    if(this.scheduled) {
      throw new Error(`Can't mark a scheduled suite.`);
    }
    if(!this.presetStatus) {
      this.presetStatus = presetStatus;
      return;
    }
    if(this.presetStatus < presetStatus) {
      this.presetStatus = presetStatus;
    }
  }

}

export class SuiteCollector {

  private readonly stored: StoredSuite[];

  public constructor() {
    this.stored = [];
  }

  public isScheduled(suite: QuerySuite) {
    const found = this.get(suite);
    return found !== undefined && found.isScheduled();
  }

  public scheduleSuite(suite: CollectedSuite) {
    const added = this.add(suite);
    added.schedule();
  }

  public getScheduled(): CollectedSuite[] {
    return this.stored.filter(i => i.isScheduled()).map(i => i.getSuite());
  }

  public countScheduled(): number {
    return this.getScheduled().length;
  }

  public markSuite(status: ErrorOutput, suite: CollectedSuite) {
    const s = this.getOrCreate(suite);
    s.mark(status);
  }

  public has(suite: QuerySuite): boolean {
    const existing = this.get(suite);
    return existing !== undefined;
  }

  public get(suite: QuerySuite): StoredSuite | undefined {
    return this.stored.find(i => i.is(suite));
  }

  private getOrCreate(suite: CollectedSuite): StoredSuite {
    const existing = this.get(suite);
    if(existing !== undefined) {
      return existing;
    }
    return this.add(suite);
  }

  private add(suite: CollectedSuite): StoredSuite {
    if(this.has(suite)) {
      throw new ReferenceError(`Collector already has ${suite.name}`);
    }
    const s = new StoredSuite(suite);
    this.stored.push(s);
    return s;
  }

}
