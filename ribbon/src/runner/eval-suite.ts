import { RibbonInitLogger, RibbonInitSectionLogger } from "../logger/init-logger";
import { TimeLogger } from "../logger/TimeLogger";
import { ErrorOutput, ErrorReaction, PrerequisiteSeverity, RunOptions } from "../RunOptions";
import { FinalizedSuite } from "../Suite";
import { SuiteCollector } from "./suite-collector";

interface CheckResultCrash {
  continueOn: false,
  crash: true,
  error: string,
}

interface NonCrashCheckResult {
  continueOn: boolean;
  crash?: false;
  warning?: string;
  error?: string;
}

type CheckResult = CheckResultCrash | NonCrashCheckResult;

function handleFailure(
  sev: PrerequisiteSeverity,
  initLog: RibbonInitLogger,
  log: RibbonInitSectionLogger,
  debugMsg: string,
  fullMsg: string,
): undefined | CheckResult {
  if(sev.duringBuild === ErrorReaction.CRASH) {
    log.error(debugMsg);
    return { continueOn: false, crash: true, error: fullMsg };
  }
  log.warn(debugMsg);
  if(!sev.runAnyways) {
    return { continueOn: false, warning: fullMsg };
  } else if(sev.duringBuild === ErrorReaction.WARN) {
    initLog.warn(fullMsg);
  }
}

function evalSuiteAfter(
  options: RunOptions,
  collected: SuiteCollector,
  suite: FinalizedSuite<any, any, any, any, any, any>,
  initLog: RibbonInitLogger,
  log: RibbonInitSectionLogger,
): CheckResult {
  const sev = options.afterSeverity;
  const after = suite.previous;

  if(after.length === 0) {
    log.debug("Skipping, no listed suites to be 'after'");
    return { continueOn: true };
  } else {
    log.log(`Should be after ${after.length} suites`);

    let correct = 0;

    for(const previousSuite of after) {
      if(collected.isScheduled(previousSuite)) {
        log.debug(`Has ${previousSuite.name}`);
        ++correct;
      } else {
        const debugMsg = `doesn't follow ${previousSuite.name}`;
        const fullMsg = `Suite ${suite.name} ${debugMsg}, as required by 'after'.`;

        if(sev.duringBuild === ErrorReaction.CRASH) {
          log.error(debugMsg);
          return { continueOn: false, crash: true, error: fullMsg };
        }
        if(sev.duringBuild !== ErrorReaction.WARN) {
          log.warn(debugMsg);
        }
        if(!sev.runAnyways) {
          return { continueOn: false, warning: fullMsg };
        } else if(sev.duringBuild === ErrorReaction.WARN) {
          initLog.warn(fullMsg);
        }
      }
    }

    log.debug(`Suite is after ${correct} of ${after.length} desired suites`);
    return { continueOn: true };
  }
}

export function evalSuiteRequireTags(
  options: RunOptions,
  suite: FinalizedSuite<any, any, any, any, any, any>,
  initLog: RibbonInitLogger,
  log: RibbonInitSectionLogger,
): CheckResult {
  const sev = options.missingTagSeverity;
  const requiredTags = options.requireTags;

  if(!requiredTags || requiredTags.length === 0) {
    log.debug("Skipping, no tags required this run.");
    return { continueOn: true };
  }

  log.log(`Checking suite has all ${requiredTags.length} required tag${requiredTags.length === 1 ? "" : "s"}`);
  let correct = 0;
  for(const tag of requiredTags) {
    const tagLog = log.subsection(tag);
    if(suite.tags.includes(tag)) {
      tagLog.debug("has tag");
      ++correct;
      continue;
    }
    const debugMsg = "missing tag";
    const res = handleFailure(
      sev, initLog, tagLog, debugMsg,
      `Suite ${suite.name} ${debugMsg} ${tag}, required by 'requiredTags'`,
    );
    if(res) {
      return res;
    }
  }
  log.debug(`Suite has ${correct} of ${requiredTags.length} required tags`);
  return { continueOn: true };
}

function evalSuiteProhibitedTags(
  options: RunOptions,
  suite: FinalizedSuite<any, any, any, any, any, any>,
  initLog: RibbonInitLogger,
  log: RibbonInitSectionLogger,
): CheckResult {
  const sev = options.hasRefusedTagSeverity;
  const prohibited = options.refuseTags;
  if(!prohibited || prohibited.length === 0) {
    log.debug("Skipping, no prohibited tags this run.");
    return { continueOn: true };
  }
  log.log(`Checking suite does not have any of the ${prohibited.length} tags`);
  let correct = 0;
  for(const tag of prohibited) {
    const tagLog = log.subsection(tag);
    if(!suite.tags.includes(tag)) {
      tagLog.debug("lacks prohibited tag");
      ++correct;
      continue;
    }

    const debugMsg = "has prohibited tag";
    const res = handleFailure(
      sev, initLog, tagLog, debugMsg,
      `Suite ${suite.name} ${debugMsg} ${tag}, prohibited by 'refuseTags'`,
    );
    if(res) { return res; }
  }
  log.debug(`Suite lacks ${correct} of ${prohibited.length} forbidden tags.`);
  return { continueOn: true };
}

export function evalSuite(
  options: RunOptions,
  collected: SuiteCollector,
  suite: FinalizedSuite<any, any, any, any, any, any>,
  initLog: RibbonInitLogger,
  log: RibbonInitSectionLogger,
): CheckResultCrash | void {
  log.logSuiteStart();
  const suiteStart = new TimeLogger();

  const afterResult = evalSuiteAfter(options, collected, suite, initLog, log.subsection('after'));
  if(afterResult.crash) {
    return afterResult;
  } else if(!afterResult.continueOn) {
    if(afterResult.warning && options.afterSeverity.duringBuild === ErrorReaction.WARN) {
      initLog.warn(afterResult.warning);
    }
    collected.markSuite(
      options.afterSeverity.output ?? ErrorOutput.SKIP,
      suite,
    );
    return;
  }

  const requireTagResult = evalSuiteRequireTags(options, suite, initLog, log.subsection('required tags'));
  if(requireTagResult.crash) {
    return requireTagResult;
  } else if(!requireTagResult.continueOn) {
    if(requireTagResult.warning && options.missingTagSeverity.duringBuild === ErrorReaction.WARN) {
      initLog.warn(requireTagResult.warning);
    }
    collected.markSuite(
      options.missingTagSeverity.output ?? ErrorOutput.SKIP,
      suite,
    );
    return;
  }

  const prohibitedTagResult = evalSuiteProhibitedTags(options, suite, initLog, log.subsection('prohibited tags'));
  if(prohibitedTagResult.crash) {
    return prohibitedTagResult;
  } else if(!prohibitedTagResult.continueOn) {
    if(prohibitedTagResult.warning && options.hasRefusedTagSeverity.duringBuild === ErrorReaction.WARN) {
      initLog.warn(prohibitedTagResult.warning);
    }
    collected.markSuite(
      options.hasRefusedTagSeverity.output ?? ErrorOutput.SKIP,
      suite,
    );
    return;
  }

  suiteStart.end();
  log.logSuiteSchedule(suiteStart.log(1, "ms"));
  collected.scheduleSuite(suite);
}
