import { CollectionMember } from "..";
import { CollectionClass, CollectionLoadStateInitiator } from "../Collection";
import { RibbonBootLogger } from "../logger/boot-logger";
import { TimeLogger } from "../logger/TimeLogger";
import { StateService } from "../state/StateService";
import { WorldActionHelpersBuilder } from "../WorldActionHelpers";

export function registerCollections<
  Collections extends CollectionClass<any>[]
>(
  stateService: StateService,
  collections: Collections,
  logger: RibbonBootLogger,
) {
  logger.debug(`Registering ${collections.length} collections.`);

  for (const Collection of collections) {
    const [collection, id] = stateService.registerCollection(Collection);
    logger.info(`Registered ${stateService.describeCollection(id)}`);
  }
}

export async function initialEntityLoad(
  stateService: StateService,
  entities: CollectionMember<any>[],
  logger: RibbonBootLogger,
) {
  logger.debug(`Loading ${entities.length} entities`);

  for(const entity of entities) {
    const itemId = stateService.registerItem(entity);
    logger.log(`Registered ${stateService.describeItem(itemId)}`);
    const helper = new WorldActionHelpersBuilder(stateService);
    const startTime = Date.now();
    const actionId = stateService.generateActionForItemInit(startTime, itemId);
    const timed = new TimeLogger();
    const started = helper.run(actionId, (t) => entity["lifecycleLoadState"](CollectionLoadStateInitiator.FIRST_RUN, t));
    if(started === undefined) {
      // didn't have anything to run
      continue;
    }
    logger.debug(`Waiting on ${stateService.describeItem(itemId)} to load initial state.`);
    const res = await started;
    timed.end();
    logger.debug(`Loaded initial state for ${stateService.describeItem(itemId)} ${timed.log(2, "s")}`);
  }
}
