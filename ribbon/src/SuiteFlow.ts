import { BuildableSuite, FinalizedSuite, Suite } from "./Suite";
import { SuiteStep, SuiteStepTask, SuiteTaskFn } from "./SuiteStep";

export type StepResults = { [name: string]: any };

export type NamedStepData<Results extends { [name: string]: any }> = {
  [StepId in keyof Results]: SuiteStepTask<any, any, any, Results[StepId]>;
}

export class SuiteFlow<
  S extends BuildableSuite<any, any, any, any, any, any, any>,
  Steps extends SuiteStep<S, any, any>[],
  Results extends { [name: string]: any },
  LastResult,
> {
  constructor(
    private readonly suite: S,
    private readonly steps: Steps,
    private readonly namedResults: NamedStepData<Results>,
  ) {}

  public step<T>(
    name: string,
    cb: SuiteTaskFn<S, Results, LastResult, T>,
  ) {
    const step = new SuiteStepTask(this.suite, this.steps, this.namedResults, name, cb);
    return step.unnamed();
  }

  public recallableStep<
    NewResult extends { [key: string]: any },
    StepName extends keyof NewResult,
    T extends NewResult[StepName],
  >(stepId: StepName, name: string, cb: SuiteTaskFn<S, Results, LastResult, T>) {
    const step = new SuiteStepTask(this.suite, this.steps, this.namedResults, name, cb);
    return step.named<NewResult, StepName>(stepId);
  }

  public complete() {
    return new FinalizedSuite(
      this.suite,
      this.steps,
      this.namedResults,
    );
  }
}
