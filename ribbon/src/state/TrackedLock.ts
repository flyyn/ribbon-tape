import { ActionRef, CollectionRef, TrackedItemRef, TrackedLockRef } from "./ref";
import { stateHash } from "./util/state-hash";
import { TrackedIdentifier } from "./util/tracked-id";

export class TrackedLockIdentifier implements TrackedIdentifier {
  public readonly typePrefix = "l";
  public readonly typeName = "Lock";

  private _hash: string | undefined;
  public constructor(
    private readonly createdAt: number,
    private readonly item: TrackedItemRef,
    private readonly action: ActionRef,
  ) {}

  public name(): string {
    const i = typeof this.item === "string" ? this.item : this.item._tracked_item_ref;
    return `on ${i}`;
  }

  public hash(): string {
    if(this._hash) {
      return this._hash;
    }
    return this._hash = stateHash(this.serializeToString());
  }

  public serializeToString(): string {
    const i = typeof this.item === "string" ? this.item : this.item._tracked_item_ref;
    const a = typeof this.action === "string" ? this.action : this.action._action_ref;
    const str = `${i};${a};${this.createdAt}`;
    return str;
  }

  public serialize(): TrackedLockRef {
    return { _lock_ref: this.serializeToString() };
  }
}

export class TrackedLock {
  private readonly id: TrackedLockIdentifier;
  private active: boolean;

  public constructor(
    id: TrackedLockIdentifier,
  ) {
    this.id = id;
    this.active = true;
  }
}
