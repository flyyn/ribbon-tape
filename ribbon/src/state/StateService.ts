import { EncounteredLockResponse } from "../action-helpers/lock/EncounteredLockResponse";
import { InternalLockResult, LockFailure } from "../action-helpers/lock/lock-result";
import { Collection, CollectionMember } from "../Collection";
import { RibbonLackUnlockPermissionError } from "../errors/test-format/RibbonLackUnlockPermissionError";
import { RibbonLockedEntityInternalError } from "../errors/test-format/RibbonLockedEntityError";
import { RibbonUnloadedCollectionError } from "../errors/test-format/RibbonUnloadedCollectionError";
import { RibbonStateManagerLogger } from "../logger/state-logger";
import { WorldLockOptions } from "../WorldActionHelpers";
import { ActionEntity, ActionRepository, StateActionId } from "./repositories/action";
import { CollectionEntity, CollectionRepository, StateCollectionId } from "./repositories/collection";
import { ItemEntity, ItemRepository, StateItemId } from "./repositories/item";
import { LockEntity, LockRepository, StateLockId } from "./repositories/lock";

const collectionKeySymbol: symbol = Symbol("collectionEntity");

export class StateService {
  private readonly actions: ActionRepository;
  private readonly collections: CollectionRepository;
  private readonly items: ItemRepository;
  private readonly locks: LockRepository;

  public constructor(
    private readonly log: RibbonStateManagerLogger,
  ) {
    this.actions = new ActionRepository();
    this.collections = new CollectionRepository();
    this.items = new ItemRepository();
    this.locks = new LockRepository();
  }

  public describeCollection(id: StateCollectionId): string {
    return id.reference(this.collections);
  }

  public describeItem(id: StateItemId): string {
    return id.reference(this.items);
  }

  public describeAction(id: StateActionId): string {
    return id.reference(this.actions);
  }

  public describeLock(id: StateLockId): string {
    return id.reference(this.locks);
  }

  public registerCollection<CollectionInstance extends Collection<any>, CollectionConstructor extends { new(): CollectionInstance }>(C: CollectionConstructor): [ CollectionInstance, StateCollectionId ] {
    const collection = new C();
    const id = StateCollectionId.fromCollection(collection);
    const entity = new CollectionEntity(id);
    this.collections.insert(id, entity);
    C[collectionKeySymbol] = id;
    return [collection, id];
  }

  public generateItemId(
    item: CollectionMember<any>,
    event: string,
  ): [StateItemId, StateCollectionId] {
    const rawCollection = item["collection"]();
    const collectionKey = rawCollection[collectionKeySymbol];
    if(!collectionKey || !(collectionKey instanceof StateCollectionId)) {
      throw new RibbonUnloadedCollectionError(
        Collection.name,
        `${event} ${item["name"]}`,
      );
    }
    const savedCollectionId: StateCollectionId = collectionKey;
    const collection = this.collections.getByIdOrFail(savedCollectionId);
    const collectionId = collection.getId();
    const itemId = new StateItemId(collectionId, item["name"]());
    return [itemId, collectionId];
  }

  public registerItem(item: CollectionMember<any>): StateItemId {
    const [id, collectionId] = this.generateItemId(item, `while registering`);
    const itemEntity = new ItemEntity(id, collectionId);
    this.items.insert(id, itemEntity);
    return id;
  }

  public generateActionForItemInit(startTime: number, item: StateItemId): StateActionId {
    const name = `Init ${this.describeItem(item)} `;
    const id = new StateActionId(startTime, name);
    const action = new ActionEntity(id);
    this.actions.insert(id, action);
    return id;
  }

  public lockItem(actionId: StateActionId, itemId: StateItemId, opts: WorldLockOptions): InternalLockResult {
    const item = this.items.getByIdOrFail(itemId);
    const existingLock = item.getExistingLock(this.locks);
    if(existingLock) {
      const previousLocker = existingLock.getLockedBy(this.actions);
      this.log.verbose(`Found existing lock for ${itemId.describe()}`);
      if(existingLock.isPreemptible() && opts.preempt) {
        this.log.trace(`Action ${actionId.describe()} is preempting ${existingLock.describe}`);
        this.deleteLock(existingLock);
      } else {
        const response = opts.ifLocked ?? EncounteredLockResponse.FAIL;
        const msg = `${this.describeItem(itemId)} is already locked and cannot be locked by ${actionId}.`;
        const failedLockInfo: LockFailure = {
          acquired: false,
          preemptible: existingLock.isPreemptible(),
          sameAction: actionId.hash === previousLocker.getId().hash,
          sameSuite: false, // TODO: get actual value
        };
        if(response === EncounteredLockResponse.CONTINUE) {
          return failedLockInfo;
        } else if(response === EncounteredLockResponse.WARN) {
          // TODO: Ensure this gets logged correctly (visibility)
          this.log.warn(msg);
          return failedLockInfo;
        } else {
          throw new RibbonLockedEntityInternalError(itemId, actionId, "locked");
        }
      }
    }
    return {
      acquired: true,
      lockId: this.createLock(actionId, itemId, opts),
    }
  }

  public removeOwnLock(actionId: StateActionId, lockId: StateLockId): void {
    const action = this.actions.getByIdOrFail(actionId);
    const lock = this.locks.getByIdOrFail(lockId);
    const lockedBy = lock.getLockedBy(this.actions);
    if(lockedBy.getId().hash !== actionId.hash) {
      throw new RibbonLackUnlockPermissionError(lock.getItemId(), actionId, "own", `lock actually created by ${lockedBy.describe()}`);
    }
    this.deleteLock(lock);
  }

  private createLock(actionId: StateActionId, itemId: StateItemId, opts: WorldLockOptions): StateLockId {
    const item = this.items.getByIdOrFail(itemId);
    const action = this.actions.getByIdOrFail(actionId);
    const timeLocked = Date.now();
    const id = new StateLockId(timeLocked, itemId, actionId);
    const lock = new LockEntity(id, itemId, actionId, {
      preemptible: opts.preemptible === true,
    });
    this.locks.insert(id, lock);
    item.lockedBy(id);
    action.createdLocks.link(id);
    return id;
  }

  private deleteLock(lock: LockEntity) {
    const id = lock.getId();
    const item = lock.getItem(this.items);
    const action = lock.getLockedBy(this.actions);
    this.log.verbose(`Deleting ${this.describeLock(id)}`);
    item.deactivateLock(this.log);
    action.createdLocks.unlink(id);
    this.locks.deleteById(id);
    this.log.trace(`Deleted ${this.describeLock(id)}`);
  }

}
