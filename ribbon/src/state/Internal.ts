import { WorldLockOptions } from "../WorldActionHelpers";
import { ActionRef, CollectionRef, TrackedItemRef } from "./ref";
import { TrackedAction } from "./TrackedAction";
import { TrackedCollection } from "./TrackedCollection";
import { TrackedItem } from "./TrackedItem";
import { TrackedLockIdentifier } from "./TrackedLock";
import { TrackedIdentifier } from "./util/tracked-id";

export type BridgeFindById<T, ID> = (id: ID) => T | undefined;
export type BridgeInsert<T, ID> = (id: ID, value: any) => T | undefined;

interface BridgeQueryProps<T, ID> {
  findById: BridgeFindById<T, ID>;
  insert: BridgeInsert<T, ID>;
};

export class BridgeQuery<T, ID> {
  public readonly findById: BridgeFindById<T, ID>;
  public readonly insert: BridgeInsert<T, ID>;

  public constructor(
    opts: BridgeQueryProps<T, ID>,
  ) {
    this.findById = opts.findById;
    this.insert = opts.insert;
  }
}

interface UniqueBridgeMethods {
  lock(actionId: TrackedIdentifier, item: TrackedItemRef, opts: WorldLockOptions): TrackedLockIdentifier | undefined;
}

export class InternalStateManagerBridge {
  public constructor(
    public readonly collectionSymbol: symbol,
    public readonly action: BridgeQuery<TrackedAction, ActionRef>,
    public readonly item: BridgeQuery<TrackedItem, TrackedItemRef>,
    public readonly collection: BridgeQuery<TrackedCollection, CollectionRef>,
    public readonly methods: UniqueBridgeMethods,
  ) {}
}
