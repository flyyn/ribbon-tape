import { ItemState } from "./ItemState";
import { StateSuiteRef } from "./ref";
import { TrackedItem } from "./TrackedItem";

export class StateChangeEvent {

  private readonly item: TrackedItem;

  private readonly suite: StateSuiteRef | false;

  private readonly confirmed: boolean;
  private readonly previousState?: ItemState;
  private readonly currentState: ItemState;

}
