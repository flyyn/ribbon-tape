export enum ItemState {
  CHECKED_OUT,
  NON_EXISTENT,
  UNKNOWN,
  EXISTS,
}
