import { Collection, CollectionClass } from "../../Collection";
import { RibbonIncompleteInputError } from "../../errors/test-format/RibbonIncompleteInputError";
import { RibbonStateDuplicateRegisterError } from "../../errors/test-format/RibbonStateDuplicateRegisterError";
import { InternalStateManagerBridge } from "../Internal";
import { TrackedCollectionIdentifier } from "../TrackedCollection";
import { TrackedIdentifier } from "../util/tracked-id";

export class CollectionStateManagerInterface {

  public constructor(
    private readonly bridge: InternalStateManagerBridge,
  ) {}

  public register(Collection: CollectionClass<any>): TrackedIdentifier {
    const collection = new Collection();
    const id = this.createId(collection);
    Collection[this.bridge.collectionSymbol] = id.serialize();
    const existing = this.bridge.collection.findById(id.serialize());
    if(existing) {
      throw new RibbonStateDuplicateRegisterError("Collection", id);
    }
    return id;
  }

  private createId(collection: Collection<any>): TrackedCollectionIdentifier {
    const ns = collection["ns"];
    const id = collection["name"];
    if(typeof ns !== "string" || ns.length < 2) {
      throw new RibbonIncompleteInputError("collection definition", collection["name"], "namespace ('ns')", "must be defined and 2+ characters long", ns);
    }
    if(typeof id !== "string" || id.length < 2) {
      throw new RibbonIncompleteInputError("collection definition", collection["name"], "id ('id')", "must be defined and 2+ characters long", id);
    }
    return new TrackedCollectionIdentifier(ns, id, collection["name"]);
  }

}
