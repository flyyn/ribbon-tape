import { Collection, CollectionMember } from "../../Collection";
import { RibbonIncompleteInputError } from "../../errors/test-format/RibbonIncompleteInputError";
import { RibbonStateDuplicateRegisterError } from "../../errors/test-format/RibbonStateDuplicateRegisterError";
import { RibbonUnloadedCollectionError } from "../../errors/test-format/RibbonUnloadedCollectionError";
import { InternalStateManagerBridge } from "../Internal";
import { CollectionRef } from "../ref";
import { TrackedCollectionIdentifier } from "../TrackedCollection";
import { TrackedItemIdentifier } from "../TrackedItem";
import { TrackedIdentifier } from "../util/tracked-id";

export class ItemStateManagerInterface {

  public constructor(
    private readonly bridge: InternalStateManagerBridge,
  ) {}

  public register(item: CollectionMember<any>, rejectIfExists = false): TrackedIdentifier {
    const id = this.createId(item);
    const existing = this.bridge.item.findById(id.serialize());
    if(rejectIfExists && existing) {
      throw new RibbonStateDuplicateRegisterError("Item", id);
    }
    return id;
  }

  public getCurrentLock(item: CollectionMember<any>) {
    const id = this.createId(item);
    const existingItem = this.bridge.item.findById(id.serialize());
    if(existingItem === undefined) {
      return undefined;
    }
  }

  private createId(item: CollectionMember<any>): TrackedItemIdentifier {
    const name = item["name"]();
    const uid = item["uid"]();
    if(typeof uid !== "string" || uid.length < 2) {
      throw new RibbonIncompleteInputError("item id", name, "id ('uid()')", "must be defined and 2+ characters long", uid);
    }
    const Collection = item["collection"]();
    const collectionId: CollectionRef = Collection[this.bridge.collectionSymbol];
    if(!collectionId) {
      throw new RibbonUnloadedCollectionError(
        Collection.name,
        `while loading item ${name}`,
      );
    }
    return new TrackedItemIdentifier(collectionId, uid, name);
  }

}
