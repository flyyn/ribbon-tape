import { Collection, CollectionClass } from "../../Collection";
import { RibbonIncompleteInputError } from "../../errors/test-format/RibbonIncompleteInputError";
import { RibbonStateDuplicateRegisterError } from "../../errors/test-format/RibbonStateDuplicateRegisterError";
import { InternalStateManagerBridge } from "../Internal";
import { TrackedActionIdentifier } from "../TrackedAction";
import { TrackedCollectionIdentifier } from "../TrackedCollection";
import { TrackedIdentifier } from "../util/tracked-id";

export class ActionStateManagerInterface {

  public constructor(
    private readonly bridge: InternalStateManagerBridge,
  ) {}

  public register<InSuite extends boolean>(
    inSuite: InSuite,
    suiteName: InSuite extends true ? string : string | undefined,
    startAt: number,
    name: string,
  ): TrackedIdentifier {
    const id = this.createId(inSuite, suiteName, startAt, name);
    const created = this.bridge.action.insert(id.serialize(), [
      id, name, startAt,
    ]);
    return id;
  }

  private createId<InSuite extends boolean>(
    inSuite: InSuite,
    suiteName: InSuite extends true ? string : string | undefined,
    startAt: number,
    name: string,
  ): TrackedActionIdentifier {
    return new TrackedActionIdentifier(inSuite, suiteName, startAt, name);
  }

}
