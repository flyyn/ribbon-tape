import { StateEntityId } from "./StateEntityId";

export class StateEntity<EntityId extends StateEntityId<any>, StoredValue> {

  protected readonly id: EntityId;

  public constructor(
    id: EntityId,
  ) {
    this.id = id;
  }

  public getId(): EntityId {
    return this.id;
  }

  public describe(): string {
    return this.id.describe();
  }

}
