import { createHash } from "crypto";
import chalk from "chalk";
import { StateRepository } from "./StateRepository";

export abstract class StateEntityId<Components extends (string | number)[]> {
  public static readonly entityPrefix: string;

  public static validate(strings: Record<string, string>, numbers?: Record<string, number>) {
    for (const strLabel in strings) {
      if (Object.prototype.hasOwnProperty.call(strings, strLabel)) {
        const strValue = strings[strLabel];
        if(!strValue || strValue.length <= 1) {
          throw new ReferenceError(`Invalid ${strLabel}: must be a string, 2 or more characters long.  (Got ${typeof strValue} '${strValue}'; ${strValue.length} long)`);
        }
      }
    }
    for (const numLabel in numbers) {
      if (Object.prototype.hasOwnProperty.call(numbers, numLabel)) {
        const numValue = numbers[numLabel];
        if(!numValue) {
          throw new ReferenceError(`Invalid ${numLabel}: must be a number.  (Got ${typeof numValue} '${numValue}')`);
        }
      }
    }
  }

  public readonly shortHash: string;
  public readonly hash: string;

  public constructor(
    private readonly components: Components,
  ) {
    const hashed = this.createHash(components);
    this.shortHash = hashed[0];
    this.hash = hashed[1];
  }

  public reference(repository: StateRepository<any, this, any>): string {
    return `${repository.describe()}#${this.describe()}`;
  }

  public describe(): string {
    return `${this.getName()} [${chalk.yellow(this.shortHash)}]`;
  }

  protected getName(): string {
    return this.components.join(";");
  }

  protected createHash(components: Components): [string, string] {
    const fullHash = this.hashArray(components);
    const shortHash = this.formatShortHash(fullHash);
    return [shortHash, fullHash];
  }

  protected formatShortHash(hash: string): string {
    const prefix: string = this.constructor["entityPrefix"];
    return `${prefix}${hash.substr(0, 5)}`;
  }

  protected hashArray(components: (string | number)[]): string {
    const hash = createHash(`sha512`);
    for (const component of components) {
      hash.update(`${component}`);
    }
    return hash.digest(`hex`);
  }

}
