import { DebugLogger } from "../../logger/debug-logger";
import { StateEntity } from "./StateEntity";
import { StateEntityId } from "./StateEntityId";
import { StateRepository } from "./StateRepository";

export enum ErrorOutputLevel {
  SILENT,
  WARN,
  ERROR,
  THROW,
}

export class EntityReference<EntityId extends StateEntityId<any>, Entity extends StateEntity<EntityId, any>> {

  public constructor(
    protected readonly source: StateEntity<any, any>,
    protected target: EntityId,
  ) {}

  public getTarget(): EntityId {
    return this.target;
  }

  public is(other: StateEntityId<any>): boolean {
    return other.hash === this.target.hash;
  }

  public get<Repository extends StateRepository<any, EntityId, Entity>>(
    repository: Repository,
  ): Entity {
    const val = repository.getById(this.target);
    if(!val) {
      throw new ReferenceError(`Error fetching ${repository.describe()} ${this.target.describe()}, referenced by ${this.source.describe()}`);
    }
    return val;
  }

}

export class MetadataEntityReference<
  EntityId extends StateEntityId<any>,
  Entity extends StateEntity<EntityId, any>,
> extends EntityReference<EntityId, Entity> {
  /**
   * Can {@link unlink} be called?
   */
  protected readonly ALLOW_DISCONNECTION: boolean = false;

  /**
   * Can {@link link} be called if the entity has (ever) been linked before?
   */
  protected readonly ALLOW_RECONNECT: boolean = false;

  /**
   * Error level if {@link unlink} called when {@link ALLOW_DISCONNECTION} is `false`.
   */
  protected readonly DISCONNECTION_ERROR: ErrorOutputLevel = ErrorOutputLevel.THROW;

  /**
   * Error level if {@link unlink} called while unlinked.
   */
  protected readonly DOUBLE_DISCONNECT: ErrorOutputLevel = ErrorOutputLevel.WARN;

  /**
   * Error level if {@link link} is called after the object has been linked once,
   * if {@link ALLOW_CONNECT} is not `true`.
   */
  protected readonly RECONNECTION_ERROR: ErrorOutputLevel = ErrorOutputLevel.THROW;

  protected readonly referenceCreated: Date;
  protected active: boolean;
  protected lastModified: Date;
  protected deactivated: Date;

  public constructor(
    source: StateEntity<any, any>,
    target: EntityId,
  ) {
    super(source, target);
    this.referenceCreated = new Date();
    this.active = true;
  }

}

export class DisconnectableEntityReference<
  EntityId extends StateEntityId<any>,
  Entity extends StateEntity<EntityId, any>,
> extends MetadataEntityReference<EntityId, Entity> {
  protected override readonly ALLOW_DISCONNECTION = true;
  protected override readonly ALLOW_RECONNECT: boolean = false;

  public isConnected(): boolean {
    return this.active;
  }

  public disconnect(log: DebugLogger): void {
    if(!this.ALLOW_DISCONNECTION) {
      const err = this.DISCONNECTION_ERROR;
      const msg = `Not allowed to disconnect reference in ${this.source.describe()} from pointing to "${this.target.describe()}"`;
      if(err === ErrorOutputLevel.THROW) { throw new ReferenceError(msg); }
      else if(err === ErrorOutputLevel.ERROR) { log.error(msg); }
      else if(err === ErrorOutputLevel.WARN) { log.warn(msg); }
    }
    if(!this.active) {
      const err = this.DOUBLE_DISCONNECT;
      const msg = `Unable to disconnect (already disconnected) in ${this.source.describe()} (previously pointed at "${this.target.describe()}")`;
      if(err === ErrorOutputLevel.THROW) { throw new ReferenceError(msg); }
      else if(err === ErrorOutputLevel.ERROR) { log.error(msg); }
      else if(err === ErrorOutputLevel.WARN) { log.warn(msg); }
    }
    this.deactivated = new Date();
    this.active = false;
  }
}


export class MutableEntityReference<
  EntityId extends StateEntityId<any>,
  Entity extends StateEntity<EntityId, any>,
> extends DisconnectableEntityReference<EntityId, Entity> {
  protected override readonly ALLOW_DISCONNECTION = true;
  protected override readonly ALLOW_RECONNECT = true;

  public connect(log: DebugLogger, target: EntityId): void {
    if(!this.ALLOW_RECONNECT) {
      const err = this.RECONNECTION_ERROR;
      const msg = `Not allowed to change reference in ${this.source.describe()} pointing to ${this.target.describe()}.`;
      if(err === ErrorOutputLevel.THROW) { throw new ReferenceError(msg); }
      else if(err === ErrorOutputLevel.ERROR) { log.error(msg); }
      else if(err === ErrorOutputLevel.WARN) { log.warn(msg); }
      return;
    }
    this.lastModified = new Date();
    this.active = true;
    this.target = target;
  }
}
