import { MetadataEntityReference } from "./entity-ref";
import { StateEntity } from "./StateEntity";
import { StateEntityId } from "./StateEntityId";

export class EntityReferenceList<
  EntityId extends StateEntityId<any>,
  Entity extends StateEntity<EntityId, any>
> {

  private items: MetadataEntityReference<EntityId, Entity>[];

  public constructor(
    private readonly source: StateEntity<any, any>,
  ) {
    this.items = [];
  }

  public link(target: EntityId) {
    this.items.push(new MetadataEntityReference(this.source, target));
  }

  public unlink(target: EntityId) {
    this.items = this.items.filter(i => !i.is(target));
  }

}
