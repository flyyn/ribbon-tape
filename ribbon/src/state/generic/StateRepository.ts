import { RibbonStateDuplicateRegisterError } from "../../errors/test-format/RibbonStateDuplicateRegisterError";
import { randomFamilyColor } from "../../util/random-color";
import { StateEntity } from "./StateEntity";
import { StateEntityId } from "./StateEntityId";

export abstract class StateRepository<StoredValue, EntityId extends StateEntityId<any>, Entity extends StateEntity<EntityId, StoredValue>> {
  public static readonly repositoryName: string;

  private readonly items: Record<string, Entity>;

  public constructor() {
    this.items = {};
  }

  protected validateInsert(id: EntityId, entity: Entity): void {}

  public insert(id: EntityId, entity: Entity): void {
    this.validateInsert(id, entity);
    if(this.items[id.hash]) {
      throw new RibbonStateDuplicateRegisterError(
        this.describe(),
        id,
        `Cannot insert duplicate ${this.describe()}#${id.describe()}.`,
      );
    }
    this.items[id.hash] = entity;
  }

  public getByIdOrFail(id: EntityId): Entity {
    const res = this.getById(id);
    if(!res) {
      throw new ReferenceError(`Couldn't find ${this.describe()}#${id.describe()}.`);
    }
    return res;
  }

  public getById(id: EntityId): Entity | undefined {
    if(this.items[id.hash]) {
      return this.items[id.hash];
    }
    return undefined;
  }

  public deleteById(id: EntityId): void {
    delete this.items[id.hash];
  }

  public describe(): string {
    const name = this.getName();
    return randomFamilyColor("orange", name, name);
  }

  protected getName(): string {
    return this.constructor["repositoryName"];
  }

}
