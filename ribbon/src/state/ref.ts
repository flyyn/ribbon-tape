export type CollectionRef = string | { _collection_ref: string };
export type TrackedItemRef = string | { _tracked_item_ref: string };
export type ActionRef = string | { _action_ref: string };
export type TrackedLockRef = string | { _lock_ref: string };
export type StateSuiteRef = string;
