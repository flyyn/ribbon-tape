import { CollectionRef, TrackedItemRef, TrackedLockRef } from "./ref";
import { stateHash } from "./util/state-hash";
import { TrackedIdentifier } from "./util/tracked-id";

export class TrackedItemIdentifier implements TrackedIdentifier {
  public readonly typePrefix = "i";
  public readonly typeName = "Item";

  private _hash: string | undefined;
  public constructor(
    private readonly collection: CollectionRef,
    private readonly id: string,
    private readonly _name: string,
  ) {}

  public name(): string {
    return this._name;
  }

  public hash(): string {
    if(this._hash) {
      return this._hash;
    }
    return this._hash = stateHash(this.serializeToString());
  }

  public serializeToString(): string {
    const c = typeof this.collection === "string" ? this.collection : this.collection._collection_ref;
    const str = `${c};${this.id}`;
    return str;
  }

  public serialize(): TrackedItemRef {
    return { _tracked_item_ref: this.serializeToString() };
  }
}

export class TrackedItem {
  private readonly id: TrackedItemIdentifier;
  private readonly name: string;
  private isLocked: boolean;
  private lastLock: TrackedLockRef;

  public constructor(
    id: TrackedItemIdentifier,
    name: string,
  ) {
    this.id = id;
    this.name = name;
    this.isLocked = false;
  }

  public currentLock(): false | TrackedLockRef {
    if(this.isLocked) {
      return this.lastLock;
    }
    return false;
  }
}
