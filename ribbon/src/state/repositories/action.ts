import { EntityReferenceList } from "../generic/entity-refs";
import { StateEntity } from "../generic/StateEntity";
import { StateEntityId } from "../generic/StateEntityId";
import { StateRepository } from "../generic/StateRepository";
import { LockEntity, StateLockId } from "./lock";

export class StateActionId extends StateEntityId<[number]> {
  public static readonly entityPrefix = "a";

  public constructor(
    private readonly timeStarted: number,
    private readonly name: string,
  ) {
    super([timeStarted]);
  }

  protected override getName() { return this.name; }
}

export class ActionEntity extends StateEntity<StateActionId, any> {
  public createdLocks: EntityReferenceList<StateLockId, LockEntity>;

  public constructor(id: StateActionId) {
    super(id);
    this.createdLocks = new EntityReferenceList(this);
  }
}

export class ActionRepository extends StateRepository<any, StateActionId, ActionEntity> {
  public static readonly repositoryName = "Action";
}
