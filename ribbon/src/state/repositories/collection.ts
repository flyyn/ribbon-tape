import { Collection } from "../../Collection";
import { StateEntity } from "../generic/StateEntity";
import { StateEntityId } from "../generic/StateEntityId";
import { StateRepository } from "../generic/StateRepository";

export class StateCollectionId extends StateEntityId<[string, string]> {
  public static override readonly entityPrefix = "c";

  public static fromCollection(collection: Collection<any>): StateCollectionId {
    return new StateCollectionId(collection["ns"], collection["name"]);
  }

  public constructor(
    private readonly ns: string,
    private readonly collectionId: string,
    private readonly collectionDisplayName: string = collectionId,
  ) {
    super([ns, collectionId]);
    StateEntityId.validate({ "namespace": ns, "collectionId": collectionId });
  }

  protected override getName() {
    return this.collectionDisplayName;
  }
}

export class CollectionEntity extends StateEntity<StateCollectionId, any> {
}

export class CollectionRepository extends StateRepository<any, StateCollectionId, CollectionEntity> {
  public static override readonly repositoryName = "Collection";
}
