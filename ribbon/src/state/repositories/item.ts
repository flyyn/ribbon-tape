import { DebugLogger } from "../../logger/debug-logger";
import { DisconnectableEntityReference, EntityReference, MutableEntityReference } from "../generic/entity-ref";
import { StateEntity } from "../generic/StateEntity";
import { StateEntityId } from "../generic/StateEntityId";
import { StateRepository } from "../generic/StateRepository";
import { ActionEntity, ActionRepository, StateActionId } from "./action";
import { CollectionEntity, StateCollectionId } from "./collection";
import { LockEntity, LockRepository, StateLockId } from "./lock";

export class StateItemId extends StateEntityId<[string, string]> {
  public static readonly entityPrefix = "i";

  public constructor(
    private readonly collection: StateCollectionId,
    private readonly name: string,
  ) {
    super([collection.hash, name]);
  }

  protected override getName() {
    return this.name;
  }

  /**
   * Abbreviated format for inclusion in other item names.
   */
  public describeShort(): string {
    return this.getName();
  }
}

export class ItemEntity extends StateEntity<StateItemId, any> {
  private readonly parentCollection: EntityReference<StateCollectionId, CollectionEntity>;
  private lastLock: undefined | DisconnectableEntityReference<StateLockId, LockEntity>;

  public constructor(
    id: StateItemId,
    parentCollection: StateCollectionId,
  ) {
    super(id);
    this.parentCollection = new EntityReference(this, parentCollection);
  }

  public lockedBy(lockId: StateLockId) {
    this.lastLock = new DisconnectableEntityReference(this, lockId);
  }

  public getExistingLock(lockRepository: LockRepository) {
    if(!this.lastLock || !this.lastLock.isConnected()) {
      return undefined;
    }
    return this.lastLock.get(lockRepository);
  }

  public deactivateLock(log: DebugLogger) {
    this.lastLock?.disconnect(log);
  }

}

export class ItemRepository extends StateRepository<any, StateItemId, ItemEntity> {
  public static readonly repositoryName = "Item";
}
