import { EntityReference } from "../generic/entity-ref";
import { StateEntity } from "../generic/StateEntity";
import { StateEntityId } from "../generic/StateEntityId";
import { StateRepository } from "../generic/StateRepository";
import { ActionEntity, ActionRepository, StateActionId } from "./action";
import { ItemEntity, ItemRepository, StateItemId } from "./item";

export class StateLockId extends StateEntityId<[number, string, string]> {
  public static readonly entityPrefix = "l";

  public constructor(
    private readonly timeLocked: number,
    private readonly item: StateItemId,
    private readonly lockedBy: StateActionId,
  ) {
    super([timeLocked, item.hash, lockedBy.hash]);
  }

  protected override getName() {
    return `${this.shortHash} for ${this.item.describeShort()}`;
  }

}

export interface LockEntityInput {
  preemptible?: boolean;
}

export interface LockEntityValue {
  isLocked: boolean;
}

export class LockEntity extends StateEntity<StateLockId, LockEntityValue> {
  private readonly item: EntityReference<StateItemId, ItemEntity>;
  private readonly lockedBy: EntityReference<StateActionId, ActionEntity>;
  private readonly preemptible: boolean;

  public constructor(
    id: StateLockId,
    item: StateItemId,
    lockedBy: StateActionId,
    input: LockEntityInput,
  ) {
    super(id);
    this.item = new EntityReference(this, item);
    this.lockedBy = new EntityReference(this, lockedBy);
    this.preemptible = input.preemptible === true;
  }

  public getItemId(): StateItemId {
    return this.item.getTarget();
  }

  public getItem(itemRepository: ItemRepository): ItemEntity {
    return this.item.get(itemRepository);
  }

  public getLockedBy(actionRepository: ActionRepository): ActionEntity {
    return this.lockedBy.get(actionRepository);
  }

  public isPreemptible(): boolean {
    return this.preemptible === true;
  }
}

export class LockRepository extends StateRepository<LockEntityValue, StateLockId, LockEntity> {
  public static readonly repositoryName = "Lock";
}
