import { CollectionRef } from "./ref";
import { stateHash } from "./util/state-hash";
import { TrackedIdentifier } from "./util/tracked-id";

export class TrackedCollectionIdentifier implements TrackedIdentifier {
  public readonly typePrefix = "c";
  public readonly typeName = "Collection";

  private _hash: string | undefined;

  public constructor(
    private readonly ns: string,
    private readonly collectionName: string,
    private readonly _name: string = collectionName,
  ) {}

  public name(): string {
    return this._name;
  }

  public hash(): string {
    if(this._hash) {
      return this._hash;
    }
    return this._hash = stateHash(this.serializeToString());
  }

  public serializeToString(): string {
    return `${this.ns};${this.collectionName}`;
  }

  public serialize(): CollectionRef {
    return { _collection_ref: this.serializeToString() };
  }
}

export class TrackedCollection {
  public constructor(
    private readonly id: TrackedCollectionIdentifier,
    private readonly name: string,
  ) {}
}
