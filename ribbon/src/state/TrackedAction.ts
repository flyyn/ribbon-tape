import { ActionRef } from "./ref";
import { stateHash } from "./util/state-hash";
import { TrackedIdentifier } from "./util/tracked-id";

export class TrackedActionIdentifier implements TrackedIdentifier {
  public readonly typePrefix = "a";
  public readonly typeName = "Action";

  private _hash: string | undefined;

  public constructor(
    private readonly inSuite: boolean,
    private readonly suiteName: string | undefined,
    private readonly startAt: number,
    private readonly _name?: string,
  ) {}

  public name() { return this._name || `untitled`; }

  public hash() {
    if(this._hash) {
      return this._hash;
    }
    return this._hash = stateHash(this.serializeToString());
  }

  public serializeToString(): string {
    return `${this.inSuite ? "y" : "n"};${this.suiteName};${this.startAt}`;
  }

  public serialize(): ActionRef {
    return { _action_ref: this.serializeToString() };
  }
}

export class TrackedAction {
  public constructor(
    private readonly id: TrackedActionIdentifier,
    private readonly name: string,
    private readonly startTime: string,
  ) {}
}
