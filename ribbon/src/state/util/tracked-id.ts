export interface TrackedIdentifier {
  typePrefix: string;
  typeName: string;
  name(): string;
  hash(): string;
  serializeToString(): string;
}
