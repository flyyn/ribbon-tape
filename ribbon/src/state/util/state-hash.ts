import { createHash } from "crypto";

export function stateHash(id: string) {
  const hash = createHash(`sha512`);
  hash.update(id);
  return hash.digest(`hex`);
}
