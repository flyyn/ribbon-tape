export enum ErrorSourceChance {
  NONE,
  RARELY,
  POTENTIAL,
  LIKELY,
  FULLY,
  UNKNOWN,
}

/**
 * A base error class for all Ribbon errors.
 * Using this directly is almost never useful - see
 * {@link RibbonValidationError},
 * {@link RibbonTestFormatError},
 * {@link RibbonInternalError}, etc.
 */
export class RibbonError extends Error {
  constructor(
    message?: string,
  ) {
    super(message);
    this.name = new.target.name;
    Object.setPrototypeOf(this, new.target.prototype);
  }
}
