import { RibbonError } from "../RibbonError";

/**
 * If you receive this, nothing's super wrong.
 *
 * This is a class to mark test assertion failures,
 * so a test should be marked as failing, but nothing's unexpected.
 */
export class RibbonValidationError extends RibbonError {

}
