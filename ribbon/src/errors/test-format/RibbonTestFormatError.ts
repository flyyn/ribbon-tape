import { ErrorSourceChance, RibbonError } from "../RibbonError";

/**
 * These errors have a chance of being caused by a faulty test,
 * not necessarily the fault of Ribbon.
 */
export class RibbonTestFormatError extends RibbonError {

  /**
   * A rough estimate of how likely this error was actually caused by an issue in Ribbon.
   */
  public readonly chanceInternal: ErrorSourceChance;

  /**
   * A rough estimate of how likely this error was external to Ribbon, and might be an issue
   * with the test itself.
   */
  public readonly chanceExternal: ErrorSourceChance;

  public constructor(
    message: string,
    chanceInternal: ErrorSourceChance = ErrorSourceChance.UNKNOWN,
    chanceExternal: ErrorSourceChance = ErrorSourceChance.UNKNOWN,
  ) {
    super(message);
    this.chanceInternal = chanceInternal;
    this.chanceExternal = chanceExternal;
  }

}
