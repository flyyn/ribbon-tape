import { ErrorSourceChance } from "../RibbonError";
import { RibbonTestFormatError } from "./RibbonTestFormatError";

const NO_FAILED_VALUE = Symbol("NoFailedValue");

function printFailed(val: any): string {
  if(val === NO_FAILED_VALUE) {
    return "";
  }
  return ` - got ${typeof val} '${val}'`;
}

export class RibbonIncompleteInputError extends RibbonTestFormatError {

  public constructor(
    public readonly category: string,
    public readonly identifier: string,
    public readonly missing: string,
    public readonly constraints?: string,
    public readonly failedValue: any = NO_FAILED_VALUE,
    message?: string,
  ) {
    super(
      message ?? `Missing ${missing} in ${category} "${identifier}"${printFailed(failedValue)} ${ constraints ? ` (${constraints})` : ''}`,
      ErrorSourceChance.NONE,
      ErrorSourceChance.FULLY,
    );
  }

}
