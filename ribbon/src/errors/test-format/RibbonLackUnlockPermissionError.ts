import { StateActionId } from "../../state/repositories/action";
import { StateItemId } from "../../state/repositories/item";
import { RibbonLockedEntityInternalError } from "./RibbonLockedEntityError";


export class RibbonLackUnlockPermissionError extends RibbonLockedEntityInternalError {

  public constructor(
    lockedItem: StateItemId,
    encounteringAction: StateActionId,
    specifics: string,
    reason: string,
    message?: string,
  ) {
    super(
      lockedItem,
      encounteringAction,
      "unlock",
      message ?? `Action ${encounteringAction.describe()} cannot unlock ${specifics} item ${lockedItem.describe()}: ${reason}`,
    );
  }

}
