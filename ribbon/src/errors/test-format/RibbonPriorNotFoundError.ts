import { ErrorSourceChance } from "../RibbonError";
import { RibbonTestFormatError } from "./RibbonTestFormatError";

export class RibbonPriorNotFoundError extends RibbonTestFormatError {

  public constructor(
    public readonly suiteTested: string,
    public readonly suiteRequested: string,
    message?: string,
  ) {
    super(
      message ?? `Suite ${suiteTested} asked to follow '${suiteRequested}', which was not found.`,
      ErrorSourceChance.POTENTIAL,
      ErrorSourceChance.LIKELY,
    );
  }

}
