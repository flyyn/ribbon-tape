import { StateEntityId } from "../../state/generic/StateEntityId";
import { TrackedIdentifier } from "../../state/util/tracked-id";
import { ErrorSourceChance } from "../RibbonError";
import { RibbonTestFormatError } from "./RibbonTestFormatError";

export class RibbonStateDuplicateRegisterError extends RibbonTestFormatError {

  public constructor(
    public readonly storeType: string,
    public readonly item: StateEntityId<any> | TrackedIdentifier,
    message?: string,
  ) {
    super(
      message ?? `Duplicate attempt to register ${storeType} item`,
      ErrorSourceChance.POTENTIAL,
      ErrorSourceChance.LIKELY,
    );
  }

}
