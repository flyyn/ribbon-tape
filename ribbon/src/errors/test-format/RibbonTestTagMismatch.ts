import { ErrorSourceChance } from "../RibbonError";
import { RibbonTestFormatError } from "./RibbonTestFormatError";

export class RibbonTestTagMismatch extends RibbonTestFormatError {
  public constructor(
    public readonly suiteTested: string,
    public readonly tag: string,
    public readonly suiteHasTag: boolean,
    message?: string,
  ) {
    super(
      message ?? `Suite ${suiteTested} ${suiteHasTag ? 'has' : 'lacks'} tag '${tag}'`,
      ErrorSourceChance.RARELY,
      ErrorSourceChance.POTENTIAL,
    );
  }
}

export class RibbonTestTagMissing extends RibbonTestTagMismatch {
  public constructor(
    suiteTested: string,
    tag: string,
    message?: string,
  ) {
    super(
      suiteTested,
      tag,
      false,
      message,
    );
  }
}

export class RibbonTestHasRefusedTag extends RibbonTestTagMismatch {
  public constructor(
    suiteTested: string,
    tag: string,
    message?: string,
  ) {
    super(
      suiteTested,
      tag,
      true,
      message ?? `Suite ${suiteTested} has refused tag '${tag}'`,
    );
  }
}
