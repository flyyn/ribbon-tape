import { ErrorSourceChance } from "../RibbonError";
import { RibbonTestFormatError } from "./RibbonTestFormatError";

export class RibbonUnloadedCollectionError extends RibbonTestFormatError {

  public constructor(
    public readonly collectionName: string,
    public readonly situation: string,
    message?: string,
  ) {
    super(
      message ?? `Unregistered collection ${collectionName} was referenced ${situation}, ensure collection is registered.`,
      ErrorSourceChance.RARELY,
      ErrorSourceChance.FULLY,
    );
  }

}
