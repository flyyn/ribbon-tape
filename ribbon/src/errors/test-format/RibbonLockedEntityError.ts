import { StateEntityId } from "../../state/generic/StateEntityId";
import { StateActionId } from "../../state/repositories/action";
import { StateItemId } from "../../state/repositories/item";
import { ErrorSourceChance } from "../RibbonError";
import { RibbonTestFormatError } from "./RibbonTestFormatError";


export class RibbonLockedEntityError extends RibbonTestFormatError {

  public constructor(
    public readonly lockedItem: string,
    public readonly encounteringAction: string,
    public readonly action: string,
    message?: string,
  ) {
    super(
      message ?? `Item ${lockedItem} already locked; cannot ${action} by ${encounteringAction}.`,
      ErrorSourceChance.NONE,
      ErrorSourceChance.FULLY,
    );
  }

}

export class RibbonLockedEntityInternalError extends RibbonLockedEntityError {

  public constructor(
    lockedItem: StateItemId,
    encounteringAction: StateActionId,
    action: string,
    message?: string,
  ) {
    super(
      lockedItem.describe(),
      encounteringAction.describe(),
      `be ${action}`,
      message,
    );
  }

}
