import { Collection, CollectionLoadStateInitiator, CollectionMember } from "./Collection";
import { WorldActionHelpers } from "./WorldActionHelpers";

interface MetadataEntryOptions<T> {
  defaultValue?: T;
  load?: (t: WorldActionHelpers) => Promise<void>;
}

export class MetadataEntry<T> extends CollectionMember<T> {
  protected override collection = () => MetadataCollection as any;

  public constructor(
    public readonly key: string,
    public readonly options: MetadataEntryOptions<T>,
  ) {
    super();
  }

  public override lifecycleLoadState(
    initiator: CollectionLoadStateInitiator,
    t: WorldActionHelpers,
  ) {
    const load = this.options.load;
    if(load) {
      return load(t);
    }
    return undefined;
  }

  protected uid(): string {
    return this.key;
  }
}

export class MetadataCollection extends Collection<MetadataEntry<any>> {
  protected override ns = "me.flyyn.ribbon.metadata";
  protected override type = MetadataEntry;
}
