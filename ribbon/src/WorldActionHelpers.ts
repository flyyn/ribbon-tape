import { CollectionMember } from ".";
import { EncounteredLockResponse } from "./action-helpers/lock/EncounteredLockResponse";
import { LockResult, LockSuccess, TestLockSuccess } from "./action-helpers/lock/lock-result";
import { RibbonLockedEntityError } from "./errors/test-format/RibbonLockedEntityError";
import { StateActionId } from "./state/repositories/action";
import { StateLockId } from "./state/repositories/lock";
import { StateService } from "./state/StateService";

export interface AbstractItemFetchOptions {
  /**
   * If `true`, the request will first attempt to secure a lock on an unlocked entity,
   * but if already locked by a non-strict user who used {@link WorldLockOptions.preemptible},
   * will break their lock to insert this instead.
   *
   * Defaults to failing on encountering any lock, preemptible or not.
   */
   preempt?: true;

   /**
    * The response to if the item is locked.
    *
    * If the item is locked in a preemptible fashion, and {@link preempt} is `true`,
    * the lock will be changed first; e.g. in that case, the action will always succeed
    * and won't be considered 'locked'.
    *
    * Defaults to {@link EncounteredLockResponse.FAIL}.
    */
   ifLocked?: EncounteredLockResponse;
}

export interface WorldLockOptions extends AbstractItemFetchOptions {
  autoReleaseOnSave?: boolean;

  autoReleaseAfterAction?: boolean;

  autoReleaseAfterNextAction?: boolean;

  autoReleaseAfterSuite?: boolean;

  /**
   * If 'true', the current action can bypass locks created by itself.
   * Otherwise, any additional attempts to access to re-lock the item will fail.
   */
  allowSelf?: true;

  /**
   * If 'true', {@link WorldActionHelpers.removeLock} can be used by you or others to remove this lock.
   * By default, this is disabled, expecting most locks to be removed in the same action.
   */
  closeableByOthers?: true;

  /**
   * If this is not a strict lock, you can allow others to take over.
   * This is intended if other users of this resource may or may not be lower priority, and allows
   * others to cleanly request the lock instead.
   */
  preemptible?: boolean;
}

type WorldHelperLockFunction<Res extends LockResult | TestLockSuccess> = <T>(item: CollectionMember<T>, opts?: WorldLockOptions) => Res;
type WorldHelperUnlockOwnFunction = (lock: TestLockSuccess) => void;

interface WorldActionHelperFunctions {
  lock: WorldHelperLockFunction<LockResult>;
  safeLock: WorldHelperLockFunction<TestLockSuccess>;
  unlock: WorldHelperUnlockOwnFunction;
}

export class WorldActionHelpersBuilder {
  constructor(
    private readonly stateService: StateService,
  ) {}

  public run<T>(
    id: StateActionId,
    userAction: (t: WorldActionHelpers) => Promise<T> | undefined,
  ): Promise<T> | undefined {
    const locks: Record<string, StateLockId> = {};
    const helpers = new WorldActionHelpers({
      lock: (i, opts) => this.lock(locks, id, i, opts),
      safeLock: (i, opts) => {
        const res = this.lock(locks, id, i, opts);
        if(!res.acquired) {
          throw new RibbonLockedEntityError(i["name"](), "the current action", "locked");
        }
        return res;
      },
      unlock: (lock) => this.unlock(locks, id, lock.lock),
    });
    const act = userAction(helpers);
    if(act === undefined) {
      return undefined;
    }
    return this.finishTestRun(id, helpers, act);
  }

  private async finishTestRun<T>(
    id: StateActionId,
    helpers: WorldActionHelpers,
    act: Promise<T>
  ): Promise<T> {
    try {
      const res = await act;
      return res;
    } catch (err) {
      throw err;
    }
  }

  private lock(
    locks: Record<string, StateLockId>,
    actionId: StateActionId,
    item: CollectionMember<any>,
    opts?: WorldLockOptions,
  ): LockResult {
    const [itemId] = this.stateService.generateItemId(item, "while attempting to locking");
    const result = this.stateService.lockItem(actionId, itemId, opts ?? {});
    if(result.acquired) {
      const lockId = result.lockId;
      const newHash = `public${lockId.hash}`;
      locks[newHash] = lockId;
      return {
        acquired: true,
        lock: newHash,
      };
    }
    return result;
  }

  private unlock(
    locks: Record<string, StateLockId>,
    actionId: StateActionId,
    lock: string,
  ) {
    const lockId = locks[lock];
    if(!lockId) {
      throw new Error(`Wrong input provided, unknown lock ${lock}`);
    }
    this.stateService.removeOwnLock(actionId, lockId);
  }
}

export class WorldActionHelpers implements WorldActionHelperFunctions {
  public readonly lock: WorldHelperLockFunction<LockResult>;
  public readonly safeLock: WorldHelperLockFunction<TestLockSuccess>;
  public readonly unlock: WorldHelperUnlockOwnFunction;

  public constructor(
    fns: WorldActionHelperFunctions,
  ) {
    this.lock = fns.lock;
    this.safeLock = fns.safeLock;
    this.unlock = fns.unlock;
  }
}
