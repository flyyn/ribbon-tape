import { BuildableSuite } from "./Suite";
import { NamedStepData } from "./SuiteFlow";
import { StepResult, SuiteStep, SuiteStepTask } from "./SuiteStep";

/**
 * Test utilities available in test steps.
 *
 * (Ribbon developer note: split out of RibbonTestUtilities because
 * "Assertions require every name in the call target to be declared with an explicit type annotation")
 */
export class RibbonAssert {
  public is<Expected>(expected: Expected, received: any): asserts received is Expected {
    if(expected !== received) {
      throw new Error();
    }
  }

  public isNumber(obj: any): asserts obj is number {
    if(typeof obj !== "number") {
      throw new Error();
    }
  }

  public isString(obj: any): asserts obj is string {
    if(typeof obj !== "string") {
      throw new Error();
    }
  }

  public isBool(obj: any): asserts obj is boolean {
    if(typeof obj !== "boolean") {
      throw new Error();
    }
  }

  public isType(obj: any, typeName: "number"): asserts obj is number
  public isType(obj: any, typeName: "string"): asserts obj is string
  public isType(obj: any, typeName: "boolean"): asserts obj is boolean
  public isType(obj: any, typeName: "number" | "string" | "boolean") {
    if(typeName === "string") {
      return this.isString(obj);
    } else if(typeName === "number") {
      return this.isNumber(obj);
    } else if(typeName === "boolean") {
      return this.isBool(obj);
    }
  }

}

/**
 * Test helpers from Ribbon.  For common assertions this will work well,
 * however you have hidden some type data by casting from {@link RibbonTestUtilities}.
 *
 * (Casting {@link RibbonTestUtilities} to {@link RibbonHelpers} does avoid the
 * "Assertions require every name in the call target to be declared with an explicit type annotation" error)
 */
export class RibbonHelpers implements RibbonAssert {
  public is<Expected>(expected: Expected, received: any): asserts received is Expected {
    if(expected !== received) {
      throw new Error();
    }
  }

  public isNumber(obj: any): asserts obj is number {
    if(typeof obj !== "number") {
      throw new Error();
    }
  }

  public isString(obj: any): asserts obj is string {
    if(typeof obj !== "string") {
      throw new Error();
    }
  }

  public isBool(obj: any): asserts obj is boolean {
    if(typeof obj !== "boolean") {
      throw new Error();
    }
  }

  public isType(obj: any, typeName: "number"): asserts obj is number
  public isType(obj: any, typeName: "string"): asserts obj is string
  public isType(obj: any, typeName: "boolean"): asserts obj is boolean
  public isType(obj: any, typeName: "number" | "string" | "boolean") {
    if(typeName === "string") {
      return this.isString(obj);
    } else if(typeName === "number") {
      return this.isNumber(obj);
    } else if(typeName === "boolean") {
      return this.isBool(obj);
    }
  }
}

export class RibbonTestUtilities<
  Suite extends BuildableSuite<any, any, any, any, any, any, any>,
  Results extends { [name: string]: any },
  LastResult,
> extends RibbonHelpers {

  public constructor(
    private readonly suite: Suite,
    private readonly namedResults: NamedStepData<Results>,
    private readonly lastResult: SuiteStep<Suite, any, LastResult>,
  ) {
    super();
  }

  public get assert(): RibbonAssert {
    return new RibbonAssert();
  }

  public testPastResults() {
    return this.namedResults;
  }

  public fromStep<K extends keyof Results>(stepId: K & keyof Results): StepResult<Results[K], boolean> {
    const step: SuiteStepTask<Suite, any, any, Results[K]> = this.namedResults[stepId];
    return step.getStepResult();
  }

}
