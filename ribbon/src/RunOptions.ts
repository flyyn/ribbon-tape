export enum ErrorReaction {
  SILENT,
  WARN,
  CRASH,
}

export enum ErrorOutput {
  /**
   * Bypass the issue like it wasn't ever intended to be part of the output.
   * Intended to be the default action for mismatching tags,
   * which usually imply the user didn't request to run the test,
   * vs. there being a mistake in the configuration.
   */
  OMIT,

  /**
   * Mark the related item like it ran successfully.
   */
  PASS,

  /**
   * Mark the related item as 'SKIP'.
   */
  SKIP,

  /**
   * Mark the related item as a failure in the test output.
   */
  FAIL,
}

/**
 * Pre-requisites like `after` or `tags` can be used for various meanings in
 * different codebases.
 *
 * This configuration allows you to customize the "severity" of each item,
 * in case your test organization means a tag not matching indicates an actual issue
 * that needs attention.
 */
export interface PrerequisiteSeverity {
  duringBuild?: ErrorReaction;
  runAnyways?: boolean;
  output?: ErrorOutput;
}

export interface RunOptions {
  describe?: boolean;
  fullDescribe?: boolean;
  preventRun?: boolean;
  requireTags?: string[];
  refuseTags?: string[];
  onlyNamePatterns?: string[];

  /**
   * The severity of a suite not following one of the suites listed in 'after'.
   *
   * Default:
   * - `duringBuild` {@link ErrorReaction.WARN}
   * - `runAnyways` `false`
   * - `output` {@link ErrorOutput.SKIP}
   */
  afterSeverity: PrerequisiteSeverity;

  /**
   * The severity of a suite not having a tag listed in {@link requireTags}.
   *
   * Default:
   * - `duringBuild` {@link ErrorReaction.SILENT}
   * - `runAnyways` `false`
   * - `output` {@link ErrorOutput.SKIP}
   */
  missingTagSeverity: PrerequisiteSeverity;

  /**
   * The severity of a suite having a tag listed in {@link refuseTags}.
   *
   * Default:
   * - `duringBuild` {@link ErrorReaction.SILENT}
   * - `runAnyways` `false`
   * - `output` {@link ErrorOutput.SKIP}
   */
  hasRefusedTagSeverity: PrerequisiteSeverity;
}
