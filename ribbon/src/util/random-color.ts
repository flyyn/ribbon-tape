import randomcolor from "randomcolor";
import chalk from "chalk";

export function randomFamilyColor(hue: string, seed: string | number, text: string): string {
  const color = randomcolor({ hue, seed });
  return chalk.hex(color)(text);
}
