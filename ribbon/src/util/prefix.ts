export type PrefixedString<Prefix extends string, Key> =
  Key extends string
    ? `${Prefix}${Key}`
    : never;
