import { CollectionMember } from "./Collection";
import { StepResults, SuiteFlow } from "./SuiteFlow";
import { SuiteOptions } from "./SuiteOptions";
import { SuiteStep, SuiteStepTask, SuiteTaskFn } from "./SuiteStep";
import { SystemAction } from "./SystemAction";
import { World } from "./World";

export type OtherSuite<W extends World<any, any, any>> = Suite<any, W, any>;

export type RequiredCollectionItems<CollectionTypes> = { [key: string]: CollectionTypes };

interface RequireSpec<
  CollectionTypes extends CollectionMember<any>,
  RequiresTypes extends CollectionTypes,
  RequiredItems extends RequiredCollectionItems<RequiresTypes>,
  LackingItems extends CollectionTypes,
  W extends World<CollectionTypes, any, any>,
  Setup extends (world: W) => SystemAction<any, any, any, any, any>,
> {
  exists: RequiredItems,
  lacks: LackingItems[],
  setup: Setup,
}

interface SuiteDef<
  CollectionTypes extends CollectionMember<any>,
  W extends World<CollectionTypes, any, any>,
  After extends OtherSuite<W>[],
  RequiresTypes extends CollectionTypes,
  RequiredItems extends RequiredCollectionItems<RequiresTypes>,
  LackingItems extends CollectionTypes,
  Setup extends undefined | ((world: W) => SystemAction<any, W, any, any, any>),
> {
  name: string;
  world: W;
  after: After;
  exists: RequiredItems,
  lacks: LackingItems[],
  setup: Setup,
}

/**
 * A set of sequential steps that are intended to run together.
 */
export class Suite<
  CollectionTypes extends CollectionMember<any>,
  W extends World<CollectionTypes, any, any>,
  After extends (OtherSuite<W>)[],
> {
  public static create<
    CollectionTypes extends CollectionMember<any>,
    W extends World<CollectionTypes, any, any>,
  >(world: W, name: string, options: SuiteOptions = {}): BuildableSuite<CollectionTypes, W, [], never, {}, never, undefined> {
    return new BuildableSuite({ world, name, after: [], exists: {}, lacks: [], setup: undefined }, options);
  }

  public constructor(
    public readonly name: string,
    public readonly World: W,
    public readonly After: After,
    protected readonly options: Readonly<SuiteOptions>,
  ) {}
}

export class BuildableSuite<
  CollectionTypes extends CollectionMember<any>,
  W extends World<CollectionTypes, any, any>,
  After extends (OtherSuite<W>)[],
  RequiresTypes extends CollectionTypes,
  RequiredItems extends RequiredCollectionItems<RequiresTypes>,
  LackingItems extends CollectionTypes,
  Setup extends undefined | ((world: W) => SystemAction<any, W, any, any, any>),
> extends Suite<CollectionTypes, W, After> {

  public constructor(
    private readonly def: SuiteDef<CollectionTypes, W, After, RequiresTypes, RequiredItems, LackingItems, Setup>,
    options: SuiteOptions,
  ) {
    super(def.name, def.world, def.after, options);
  }

  /**
   * Ensure this suite follows a different suite.
   */
  public follows<S extends OtherSuite<W>>(suite: S): BuildableSuite<
    CollectionTypes,
    W,
    [...After, S],
    RequiresTypes,
    RequiredItems,
    LackingItems,
    Setup
  > {
    return new BuildableSuite({
      ...this.def,
      after: [ ...this.def.after, suite ],
    }, this.options);
  }

  public require<
    _RequiresTypes extends CollectionTypes,
    _RequiredItems extends RequiredCollectionItems<_RequiresTypes>,
    _LackingItems extends CollectionTypes,
    _Setup extends (world: W) => SystemAction<any, W, any, any, any>
  >(spec: RequireSpec<CollectionTypes, _RequiresTypes, _RequiredItems, _LackingItems, W, _Setup>): BuildableSuite<
    CollectionTypes,
    W,
    After,
    _RequiresTypes,
    _RequiredItems,
    _LackingItems,
    _Setup
  > {
    return new BuildableSuite({
      ...this.def,
      exists: spec.exists,
      lacks: spec.lacks,
      setup: spec.setup,
    }, this.options);
  }

  public step<T>(
    name: string,
    cb: SuiteTaskFn<this, {}, undefined, T>,
  ): SuiteFlow<this, [...never[], SuiteStepTask<this, never[], {}, T>], {}, T> {
    const step = new SuiteStepTask(this, [], {}, name, cb);
    return step.unnamed();
  }

}

export class FinalizedSuite<
  CollectionTypes extends CollectionMember<any>,
  W extends World<CollectionTypes, any, any>,
  After extends (OtherSuite<W>)[],
  Steps extends SuiteStep<any, any, any>[],
  Results extends StepResults,
  FinalResult,
> extends Suite<CollectionTypes, W, After> {

  constructor(
    private readonly builtSuite: BuildableSuite<CollectionTypes, W, After, any, any, any, any>,
    private readonly steps: Steps,
    private readonly results: Results,
  ) {
    super(builtSuite.name, builtSuite.World, builtSuite.After, builtSuite["options"]);
  }

  public get tags(): string[] {
    return this.options.tags ?? [];
  }

  public get previous(): OtherSuite<W>[] {
    return this.builtSuite["def"].after;
  }

  private get requiredItemsToExist(): RequiredCollectionItems<CollectionTypes> {
    return this.builtSuite["def"].exists;
  }

  private get requiredItemsToLack(): CollectionTypes[] {
    return this.builtSuite["def"].lacks;
  }

  private get setup(): undefined | ((world: W) => SystemAction<any, W, any, any, any>) {
    return this.builtSuite["def"].setup;
  }

}
