import { World } from "./World";

export type SystemActionResult = { [actionId: string]: any };

export class SystemAction<
  Results extends SystemActionResult,
  W extends World<any, any, any>,
  LastInput extends any[],
  LastAction extends keyof Results,
  LastResult extends Results[LastAction],
> {

  public constructor(
    protected readonly world: W,
    protected readonly actionId: LastAction,
    protected readonly execute: (...args: LastInput) => Promise<LastResult>,
  ) {}

}

export class FirstSystemAction<
  Results extends SystemActionResult,
  W extends World<any, any, any>,
  LastAction extends keyof Results,
  LastResult extends Results[LastAction],
> extends SystemAction<Results, W, [], LastAction, LastResult> {}

export class QueuedSystemAction<
  Results extends SystemActionResult,
  W extends World<any, any, any>,
  LastAction extends keyof Results,
  LastResult extends Results[LastAction],
  PrevAction extends keyof Omit<Results, LastAction>,
  PrevResult extends Results[PrevAction],
  LastInput extends any[],
> extends SystemAction<Results, W, LastInput, LastAction, LastResult> {

  public constructor(
    protected readonly previous: SystemAction<Omit<Results, LastAction>, W, any, PrevAction, PrevResult>,
    world: W,
    actionId: LastAction,
    execute: (...args: LastInput) => Promise<LastResult>,
  ) {
    super(world, actionId, execute);
  }

}
