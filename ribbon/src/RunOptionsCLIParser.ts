import arg from "arg";
import { ErrorOutput, ErrorReaction, RunOptions } from "./RunOptions";

interface ReactionArg {
  arg: (value: string, argName: string, previous?: ErrorReaction) => ErrorReaction;
  get: () => ErrorReaction;
}

function buildReactionArg(defaultReaction: ErrorReaction): ReactionArg {
  let val = defaultReaction;
  const arg = (value: string, argName: string, previousValue: ErrorReaction | undefined) => {
    if(value === "crash") {
      val = ErrorReaction.CRASH;
    } else if(value === "warn") {
      val = ErrorReaction.WARN;
    } else if(value === "silent") {
      val = ErrorReaction.SILENT;
    } else {
      throw new ReferenceError(`'${value}' is not a valid option for ${argName}.  Options: 'crash', 'warn', 'silent'.`);
    }
    return val;
  };
  const get = () => val;
  return { arg, get };
}

interface ErrorOutputArg {
  arg: (value: string, argName: string, previous?: ErrorOutput) => ErrorOutput;
  get: () => ErrorOutput;
}

function buildErrorOutputArg(defaultOutput: ErrorOutput): ErrorOutputArg {
  let val = defaultOutput;
  const arg = (value: string, argName: string, previousValue?: ErrorOutput) => {
    if(value === "fail") {
      val = ErrorOutput.FAIL;
    } else if(value === "skip") {
      val = ErrorOutput.SKIP;
    } else if(value === "pass") {
      val = ErrorOutput.PASS;
    } else if(value === "omit") {
      val = ErrorOutput.OMIT;
    } else {
      throw new ReferenceError(`'${value} is not a valid option for ${argName}.  Options: 'fail', 'skip', 'pass', 'omit'.`);
    }
    return val;
  };
  const get = () => val;
  return { arg, get };
}

export function RunOptionsCLIParser(argv = process.argv): RunOptions {
  const afterSeverityBuild = buildReactionArg(ErrorReaction.WARN);
  const afterSeverityOutput = buildErrorOutputArg(ErrorOutput.SKIP);
  const missingTagSeverityBuild = buildReactionArg(ErrorReaction.SILENT);
  const missingTagSeverityOutput = buildErrorOutputArg(ErrorOutput.SKIP);
  const refusedTagSeverityBuild = buildReactionArg(ErrorReaction.SILENT);
  const refusedTagSeverityOutput = buildErrorOutputArg(ErrorOutput.SKIP);
  const args = arg({
    '--describe': Boolean,
    '--full-describe': Boolean,
    '--and-run': Boolean,

    '--failed-after-build-reaction': afterSeverityBuild.arg,
    '--continue-after-failed-after': Boolean,
    '--failed-after-output': afterSeverityOutput.arg,

    '--missing-tag-build-reaction': missingTagSeverityBuild.arg,
    '--continue-after-missing-tag': Boolean,
    '--missing-tag-output': missingTagSeverityOutput.arg,

    '--refused-tag-build-reaction': refusedTagSeverityBuild.arg,
    '--continue-after-refused-tag': Boolean,
    '--refused-tag-output': refusedTagSeverityOutput.arg,

    '--must-have-tag': [String],
    '--cant-have-tag': [String],
    '--tag': [String],

    '-d': '--describe',
    '-e': '--must-have-tag',
    '-x': '--cant-have-tag',
  }, { argv });

  const describe = args["--describe"] === true;
  const fullDescribe = args["--full-describe"] === true;

  const anyAlternativeMode = describe || fullDescribe;

  return {
    describe,
    fullDescribe,
    preventRun: !anyAlternativeMode || args["--and-run"],

    requireTags: args["--must-have-tag"],
    refuseTags: args["--cant-have-tag"],

    afterSeverity: {
      duringBuild: afterSeverityBuild.get(),
      runAnyways: args['--continue-after-failed-after'] === true,
      output: afterSeverityOutput.get(),
    },

    missingTagSeverity: {
      duringBuild: missingTagSeverityBuild.get(),
      runAnyways: args['--continue-after-missing-tag'] === true,
      output: missingTagSeverityOutput.get(),
    },

    hasRefusedTagSeverity: {
      duringBuild: refusedTagSeverityBuild.get(),
      runAnyways: args['--continue-after-missing-tag'] === true,
      output: refusedTagSeverityOutput.get(),
    },
  };
}
