import { RibbonAssert } from ".";
import { RibbonTestUtilities } from "./RibbonTestUtilities";
import { BuildableSuite, Suite } from "./Suite";
import { NamedStepData, SuiteFlow } from "./SuiteFlow";

export class StepResult<T, Success extends boolean> {
  public constructor(
    public readonly successful: Success,
    public readonly result: Success extends true ? T : undefined,
  ) {}
}

export class SuiteStep<
  S extends Suite<any, any, any>,
  PreviousSteps extends SuiteStep<any, any, any>[],
  Results extends { [name: string]: any },
> {
  protected hasRun: boolean;
  protected runSuccess: undefined | boolean;

  public constructor(
    protected readonly suite: S,
    protected readonly previous: PreviousSteps,
    protected readonly namedSteps: NamedStepData<Results>,
  ) {
    this.hasRun = false;
  }
}

export type SuiteTaskFn<
  Suite extends BuildableSuite<any, any, any, any, any, any, any>,
  Results extends { [name: string]: any },
  LastResult,
  ReturnType,
> =
  (t: RibbonTestUtilities<Suite, Results, LastResult>, data: LastResult, assert: RibbonAssert) => Promise<ReturnType>;

export class SuiteStepTask<
  S extends BuildableSuite<any, any, any, any, any, any, any>,
  PreviousSteps extends SuiteStep<any, any, any>[],
  Results extends { [name: string]: any },
  ReturnType,
> extends SuiteStep<S, PreviousSteps, Results> {
  protected runResult: undefined | ReturnType;

  public constructor(
    suite: S,
    previous: PreviousSteps,
    namedSteps: NamedStepData<Results>,
    private readonly name: string,
    private readonly cb: SuiteTaskFn<S, Results, any, ReturnType>,
  ) {
    super(suite, previous, namedSteps);
  }

  public unnamed(): SuiteFlow<S, [...PreviousSteps, this], Results, ReturnType> {
    return new SuiteFlow(this.suite, [...this.previous, this], this.namedSteps);
  }

  public named<
    NewRecord extends { [name: string]: ReturnType },
    Key extends keyof NewRecord,
  >(name: Key): SuiteFlow<S, [...PreviousSteps, this], Results & NewRecord, ReturnType> {
    const namedData: NamedStepData<Results & NewRecord> = {
      ...this.namedSteps,
      [name]: this,
    } as any;
    return new SuiteFlow(this.suite, [...this.previous, this], namedData);
  }

  public getStepResult(): StepResult<ReturnType, boolean> {
    if(!this.hasRun) {
      throw new Error("Recalling data from step that hasn't run yet");
    }
    if(this.runSuccess === undefined) {
      throw new Error("Step ran, but didn't have a success value.");
    }
    if(this.runSuccess) {
      if(!this.runResult) {
        throw new Error("Step ran, but didn't have a result as promised.");
      }
      return new StepResult(true, this.runResult);
    } else {
      return new StepResult<ReturnType, false>(false, undefined);
    }
  }
}
