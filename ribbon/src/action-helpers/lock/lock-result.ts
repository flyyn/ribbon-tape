import { StateLockId } from "../../state/repositories/lock";

export interface LockFailure {
  acquired: false;
  preemptible?: boolean;
  sameAction: boolean;
  sameSuite: boolean;
}

export interface LockSuccess {
  acquired: true;
}

export interface TestLockSuccess extends LockSuccess {
  lock: string;
}

export type LockResult = LockFailure | TestLockSuccess;

export interface LockSuccessInternal extends LockSuccess {
  lockId: StateLockId;
}

export type InternalLockResult = LockFailure | LockSuccessInternal;
