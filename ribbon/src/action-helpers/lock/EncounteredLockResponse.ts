export enum EncounteredLockResponse {
  /**
   * (default) - lock is essential, so fail if a lock cannot be established.
   */
  FAIL = "fail",

  /**
   * Can handle things if we aren't granted a lock, continue anyways.
   * (Information returned from the request will mark the data was locked)
   */
  CONTINUE = "continue",

  /**
   * Variant of {@link CONTINUE} - displays a system log (like {@link FAIL}),
   * then
   */
   WARN = "warn",
}
