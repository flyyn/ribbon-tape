import { WorldActionHelpers } from "./WorldActionHelpers";

/**
 * The reason a collection is being asked to load state.
 */
export enum CollectionLoadStateInitiator {
  FIRST_RUN,
  SUITE_START,
}

export abstract class CollectionMember<T> {
  protected abstract collection<T extends this>(): CollectionClass<T>;

  /**
   * A display name for this item - can be readable, doesn't need to be unique.
   */
  protected name(): string {
    return this.uid();
  }

  /**
   * Allows an initial loading of the items state.
   * This could allow you to ensure that common objects are in the assumed state.
   *
   * This is intended to be run once, but future changes may have options to re-run
   * loading the state, intended to help if the state may become impossible to track.
   *
   * Parameter {@link initiator} is intended to help distinguish between the initial load
   * and reloads.
   *
   * @param initiator The reason why the state is being loaded
   * @param t Utilities for loading world state
   * @returns a promise resolving once state has been [re]loaded, or `undefined` if you do not have anything to load.
   */
  protected lifecycleLoadState(
    initiator: CollectionLoadStateInitiator,
    t: WorldActionHelpers,
  ): Promise<void> | undefined {
    return undefined;
  }

  /**
   * Items must define a unique identifier that can pick exactly this item.
   * Must be unique inside the related {@link Collection}, can conflict with others.
   */
  protected abstract uid(): string;
}

interface CollectionMemberConstructor<T extends CollectionMember<any>, Args extends any[]> {
  new(...args: Args): T;
}

/**
 * Abstract representation part of a world that contains multiple similar entities.
 *
 * Could be used for:
 * - Rows in a table
 * - Files written to disk
 */
export abstract class Collection<T extends CollectionMember<any>> {
  protected abstract type: CollectionMemberConstructor<T, any[]>;
  protected readonly abstract ns: string;
  protected readonly name: string;

  public constructor(name?: string) {
    if(!this.name) {
      this.name = name ?? this.constructor.name;
    }
  }

  /**
   * Perform initial setup.
   *
   * This is intended for tasks like loading basic files and configuration
   * or opening network connections, and not for loading initial data (see {@link lifecycleLoadState}).
   *
   * @returns a promise that resolves once this collection is ready.
   */
  public lifecycleSetup(): Promise<void> {
    return Promise.resolve();
  }

  /**
   * Allows an initial loading of the collection's state.
   * This could allow you to ensure that common objects are in the assumed state.
   *
   * This is intended to be run once, but future changes may have options to re-run
   * loading the state, intended to help if the state may become impossible to track.
   *
   * Parameter {@link initiator} is intended to help distinguish between the initial load
   * and reloads.
   *
   * @param initiator The reason why the state is being loaded
   * @returns a promise resolving once state has been [re]loaded.
   */
  public lifecycleLoadState(initiator: CollectionLoadStateInitiator): Promise<void> {
    return Promise.resolve();
  }

}

export type CollectionClass<T extends CollectionMember<any>> = {
  //type(): CollectionMemberConstructor<T>,
  new (): Collection<T>,
};
